#!/usr/bin/env node

/**
 * Copyright 2015-2016 by Jesse Jaara <jesse.jaara@gmail.com>
 * Copyright 2015-2016 by Jussi Karhu <karhu.jussi.t@student.uta.fi>
 *
 * This file is part of IVEditor.
 *
 * IVEditor is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * IVEditor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with IVEditor.
 * If not, see <http://www.gnu.org/licenses/>.
 *
 * @license GPL-3.0+ <http://spdx.org/licenses/GPL-3.0+>
 */

'use strict';

let cmd = require('commander');
let events = require('events');
let fs = require('fs');
let util = require('util');
let utils = require('./lib/utils.js');
let open = require('opener');
let Project = require('./lib/project.js');
let Server = require('./lib/server.js');

let __ = require('./lib/gettext-helper.js');
let fmt = util.format;
let emitter = new events.EventEmitter();

function validateFrontendPath(path) {
    let fullPath = path + '/html/IVEditor.html';
    try {
        fs.accessSync(fullPath, fs.R_OK);
    } catch (error) {
        if (error) {
            let reason = utils.parseFsError(error);
            emitter.emit('optionsError', fmt(__("Cannot load frontend from path '%s'.\nReason: %s"), path, reason));
        }
    }

    return path;
}

function setUpOptions() {
    cmd.version('0.0.1')
        .usage('[options]')
        .option('-f --frontend-path <path>',
                __('Location of frontend files'),
                validateFrontendPath,
                __dirname)
        .option('-p --port <number>',
                __('Port to run the http server on (8000)'),
                parseInt,
                8000)
        .option('-l --launch [editor/viewer]',
                __('Launch the editor or viewer in webbrowser (editor)'),
                /^(editor|viewer)$/i,
                'editor')
        .option('-r --project <path>',
                __('Load project from given path or init a new one if not found'))
        .option('-d --devel',
                __('Be more verbose'))
        .parse(process.argv);
}

function exitWithError(error) {
    console.log(error);
    process.exit(1);
}

// region: main

emitter.on('optionsError', exitWithError);
setUpOptions();

let server = new Server(cmd);

if (cmd.project) {
    server.setProject(new Project(fs.realpathSync(cmd.project)));

    if (cmd.launch === 'viewer') {
        open(`http://localhost:${cmd.port}/html/IVViewer.html`);
    } else {
        open(`http://localhost:${cmd.port}/html/IVEditor.html`);
    }
} else {
    open(`http://localhost:${cmd.port}/browser`);
}

// endregion: main
