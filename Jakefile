'use strict';

let buffer = require('buffer');
let bytes = require('bytes');
let child_process = require('child_process');
let colors = require('colors');
let Download = require('download');
var downloadStatus = require('download-status');
let fs = require('fs-extra');
let installDependencies = require('./bin/install_dependencies.js');
let path = require('path');
let process = require('process');
let yazl = require('yazl');

function status(str) {
    jake.logger.log(str.yellow);
}

function finish(str) {
    jake.logger.log(str.green);
}

function error(str) {
    jake.logger.error(str.red);
}

function pkgver() {
    return fs.readJsonSync('package.json').version;
}

function nodeVersion() {
    return fs.readFileSync('bundle/node.version');
}

function ZipStatus(iss, oss) {
    this.totalBytes = 0;
    this.chars = ['│', '│', '╱', '╱', '─', '─', '╲', '╲']; // Double chars to make it spin a bit slower
    this.charIndex = 0;

    iss.on('end', () => oss.end());
    iss.on('data', chunk => {
        oss.write(chunk);

        this.totalBytes += chunk.length;

        let char = this.chars[this.charIndex % 8];
        let size = bytes.format(this.totalBytes, {fixedDecimal: true, thousandsSeparator: ''});
        size = ('         ' + size).slice(-9);

        process.stdout.write(`Writing zip file: ${char} ${size} written.\x1b[0G`);
        this.charIndex++;
    });
}
ZipStatus.prototype.constructor = ZipStatus;

function promiseGeneralZipfile(zipRoot) {
    return new Promise((resolve, reject) => {
        let zf = new yazl.ZipFile();
        let walks = [];

        for (let dir of ['assets', 'css', 'html', 'template', 'js', 'lib', 'bundle/node_modules']) {
            walks.push(new Promise(solve => {
                let files = [];
                let w = fs.walk(dir);
                w.on('end', () => solve(files));
                w.on('data', item => {
                    if (item.stats.isFile()) {
                        files.push(path.relative('.', item.path));
                    }
                });
            }));
        }

        Promise.all(walks).then(lists => {
            for (let file of [].concat(...lists)) {
                zf.addFile(file, zipRoot + file.replace('bundle/', ''));
            }

            zf.addFile('IVEditor.js', zipRoot + 'IVEditor.js');
            resolve(zf);
        });
    });
}

function combineCodeFiles() {
    status('Combining individual IVViewer modules.')
    return new Promise((resolve, reject) => {
        let chunks = [];
        let r = child_process.spawn('node', ['node_modules/requirejs/bin/r.js', '-o', 'build.js']);

        r.stdout.on('data', chunk => chunks.push(chunk));
        r.on('close', ecode => {
            if (ecode !== 0) {
                reject('Failed to run requirejs optimizer.');
            } else {
                resolve(buffer.Buffer.concat(chunks).toString().replace('var self = self || {}', ''));
            }
        });
    });
}

task('installFrontendDeps', [], {async: true}, function installFrontendDeps() {
    status('Making sure all frontend dependencies are installed.');
    jake.exec(['npm install --only=dev'], {interactive: true}, function feInstallDeps() {
        installDependencies().then(() => {
            finish('All frontend dependencies have been installed.');
            complete();
        });
    });
});

file('.feInstalled', [], {async: true}, function() {
    let t = jake.Task.installFrontendDeps;
    t.addListener('complete', () => {
        fs.writeFileSync('.feInstalled', '');
        complete();
    });
    t.invoke();
});

task('bundleModules', [], {async: true}, function installBackendDependencies() {
    status('Installing all backend dependencies to clean node_modules directory.');
    fs.readJson('package.json', (err, pkg) => {
        let cmd = 'npm install --prefix ./bundle';
        for (let dep of Object.keys(pkg.dependencies)) {
            let version = pkg.dependencies[dep];
            cmd += ` ${dep}@"${version}"`;
        }

        jake.exec([cmd], {interactive: true}, function beInstallDeps() {
            finish('All backend dependencies have been installed');
            complete();
        });
    });
});

file('.modulesInstalled', [], {async: true}, function() {
    let t = jake.Task.bundleModules;
    t.addListener('complete', () => {
        fs.writeFileSync('.modulesInstalled', '');
        complete();
    });
    t.invoke();
});

task('findNodeVersion', [], {async: true}, function() {
    status('Querying internet for newest Node.js version.');
    fs.ensureDir('bundle/download/', function() {
        new Download()
            .get('https://nodejs.org/dist/latest-v5.x/')
            .dest('bundle/download')
            .use(downloadStatus())
            .run(() => complete());
    });
});

file('bundle/node.version', [], {async: true}, function parseVersion() {
    let t = jake.Task.findNodeVersion;
    t.addListener('complete', () => {
        status('Resolving newest Node.js version.');
        fs.readFile('bundle/download/latest-v5.x', {encoding: 'utf8'}, (err, data) => {
            if (err) {
                error('Cannot determinate current Node JS version.');
                error('Failed to open file bundle/download/latest-v5.x.');
                error(`Reason: ${error}`);
            } else {
                let re = /node-(v5\.\d+\.\d+)\.tar.gz/;
                let version = re.exec(data)[1];
                finish(`Newest Node.js version is ${version}`);
                fs.writeFile('bundle/node.version', version, () => complete());
            }
        });
    });

    t.invoke();
});

file('bundle/download/node32.exe', [], {async: true}, function downloadW32() {
    status('Downloading 32bit node executable for windows.');
    new Download()
        .get('https://nodejs.org/dist/latest-v5.x/win-x86/node.exe')
        .rename('node32.exe')
        .use(downloadStatus())
        .dest(`bundle/download`)
        .run(() => {
            finish('32bit Node.js executable downloaded.');
            complete();
        });
});

file('bundle/download/node64.exe', [], {async: true}, function downloadW64() {
    status('Downloading 64bit node executable for windows.');
    new Download()
        .get('https://nodejs.org/dist/latest-v5.x/win-x64/node.exe')
        .rename('node64.exe')
        .use(downloadStatus())
        .dest(`bundle/download`)
        .run(() => {
            finish('64bit Node.js executable downloaded.');
            complete();
        });
});

let bundleWindowsPrerequisites = [
    '.feInstalled',
    '.modulesInstalled',
    'bundle/download/node64.exe',
    'bundle/download/node32.exe'
];

task('bundleWindows', bundleWindowsPrerequisites, {async: true}, function bundleWindows() {
    status('Building zip bundle for Windows');
    let version = pkgver();
    promiseGeneralZipfile('IVEditor/').then(zf => {
        zf.addFile('bundle/download/node32.exe', 'IVEditor/node32.exe');
        zf.addFile('bundle/download/node64.exe', 'IVEditor/node64.exe');
        zf.addFile('dist/windows/launcher.exe', 'IVEditor/IVEditor.exe');

        let bundleFile = `bundle/IVEditor-${version}-windows.zip`;
        let out = fs.createWriteStream(bundleFile);
        out.on('finish', () => {
            finish(`Windows package has finished building. It is available at: ${bundleFile}`);
            complete();
        });
        let zfStatus = new ZipStatus(zf.outputStream, out);
        zf.end();
    });
});

file('bundle/download/node.osx', ['bundle/node.version'], {async: true}, function downloadOSX() {
    status('Downloading Node.js executable for OS X.');
    let nodeVers = nodeVersion();
    new Download({extract: true})
        .get(`https://nodejs.org/dist/latest-v5.x/node-${nodeVers}-darwin-x64.tar.gz`)
        .use(downloadStatus())
        .dest('bundle/download')
        .run(() => {
            status('OS X Node.js executable downloaded.');
            fs.copy(`bundle/download/node-${nodeVers}-darwin-x64/bin/node`, 'bundle/download/node.osx', () => {
                finish('OS X Node.js extracted and placed in bundle/download');
                complete();
            });
        });
});

let bundleOSXPrerequisites = [
    '.feInstalled',
    '.modulesInstalled',
    'bundle/download/node.osx'
];

task('bundleOSX', bundleOSXPrerequisites, {async: true}, function bundleOSX() {
    status('Building zipped app bundle for OS X');
    let version = pkgver();
    let zipRoot = 'IVEditor.app/Contents/Resources/';
    promiseGeneralZipfile(zipRoot).then(zf => {
        zf.addFile('bundle/download/node.osx', zipRoot + 'node');
        zf.addFile('dist/osx/IVEditor.app/Contents/Resources/IVEditor.icns', zipRoot + 'IVEditor.icns');
        zf.addFile('dist/osx/IVEditor.app/Contents/Info.plist' , 'IVEditor.app/Contents/Info.plist');
        zf.addFile('dist/osx/IVEditor.app/Contents/MacOS/IVEditor', 'IVEditor.app/Contents/MacOS/IVEditor');

        let bundleFile = `bundle/IVEditor-${version}-osx.zip`;
        let out = fs.createWriteStream(bundleFile);
        out.on('finish', () => {
            finish(`OS X bundle has finished building. It is available at: ${bundleFile}`);
            complete();
        });
        let zfStatus = new ZipStatus(zf.outputStream, out);
        zf.end();
    });
});

task('bundle', ['bundleWindows', 'bundleOSX'], function bundle() {
    finish('All bundles build!');
    finish('You can find them in bundles directory!');
});

task('clean', [], function clean() {
    fs.removeSync('.feIntalled');
    fs.removeSync('.modulesInstalled');
    fs.removeSync('bundle');
});

task('buildViewer', ['.feInstalled'], {async: true}, function build() {
    status('Compiling and uglifying IVViewer code.');

    let closure = child_process.spawn('closure', ['--language_out=ECMASCRIPT5', '--compilation_level=SIMPLE']);
    let uglify = child_process.spawn('node', [
        'node_modules/uglifyjs/bin/uglifyjs',
        '-',
        '-m',
        '-r=IVConfig',
        '-c',
        '--screw-ie8',
        '-o',
        'dist/compiled_ivviewer.js'
    ]);

    closure.stdout.on('data', chunk => uglify.stdin.write(chunk));
    closure.stderr.on('data', chunk => error(chunk.toString()));
    closure.stdin.on('finish', () => status('Uglifying ES5 code.'));
    closure.on('close', ecode => {
        if (ecode !== 0) {
            error('Closure failed to compile ES6 to ES5 code.');
        }
        uglify.stdin.end();
    });

    uglify.stderr.on('data', chunk => error(chunk.toString()));
    uglify.on('close', ecode => {
        if (ecode !== 0) {
            error('Uglify failed to uglify closure output.');
        } else {
            finish('Code compiled and uglified!');
            complete();
        }
    });

    combineCodeFiles().then(result => {
        status('Compiling ES6 code to ES5 code with Closure.');
        closure.stdin.end(result);
    }).catch(reason => error(reason));
});
