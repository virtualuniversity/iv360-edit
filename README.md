# V360-Edit

[![Build Status](http://jenkins.sis.uta.fi/jenkins/buildStatus/icon?job=Virtual%20University)](http://jenkins.sis.uta.fi/jenkins/job/Virtual University)

V360-Edit allows you to add interactive content to your 360° videos,
making them even more astounishing. 

Editor for adding interactive content to 360⁰ videos.

Checkout a live demo project at [http://utatour.tk/](http://utatour.tk/).

# More info

Techinal documentation for a sturcture of an IVProject and editor's export format:
[IVProject specifications sheet](https://bitbucket.org/virtualuniversity/documents/downloads/Virtual%20University:%20Video%20Project%20Dataformat%20Specification.pdf)

User manual for the editor and the viewer:
[manual](https://bitbucket.org/virtualuniversity/documents/downloads/IVEditor%20manual.pdf)

# Buidling the app
## For local system

~~~~~
npm install
node bin/install_dependencies.js
~~~~~

## Building redistributable packages

~~~~~
jake bundle
~~~~~

Will create redistributable packages for Windows and OS X in the budle directory.

# System Requirenments

* Node.js > 5.0.0
    - Only 5.4.0 has been tested. Newers versions of 5.0.0 branch should just work.

* A modern web browser (Firefox 43, Chromium 47, Edge 13) that supports
    - PointerLock API
    - WebAudio API
    - WebGL 1.0
    - Partial support for ES6
        * Arrow functions
        * Template string literals
        * Maps
