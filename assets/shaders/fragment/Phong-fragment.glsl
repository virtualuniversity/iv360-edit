/* Based on Three.js's PhonMaterial shader. http://threejs.org/ */

#extension GL_OES_standard_derivatives : enable

#define TWO_THIRDS 0.6666666666

precision mediump float;

uniform mat4 viewMatrix;
uniform vec3 cameraPosition;

uniform vec3 specular;

uniform sampler2D map;
uniform sampler2D normalMap;
uniform sampler2D specularMap;

varying vec2 vUv;
varying vec3 vViewPosition;
varying vec3 vNormal;

float calcLightAttenuation(float lightDistance, float cutoffDistance, float decayExponent) {
    if (decayExponent > 0.0) {
      return pow(clamp(-lightDistance / cutoffDistance + 1.0, 0.0, 1.0), decayExponent);
    }

    return 1.0;
}

vec3 F_Schlick(in vec3 specularColor, in float dotLH) {
    // Original approximation by Christophe Schlick '94
    //;float fresnel = pow(1.0 - dotLH, 5.0);

    // Optimized variant (presented by Epic at SIGGRAPH '13)
    float fresnel = exp2((-5.55437 * dotLH - 6.98316) * dotLH);

    return (1.0 - specularColor) * fresnel + specularColor;
}

float D_BlinnPhong(in float shininess, in float dotNH) {
    // factor of 1/PI in distribution term omitted

    return (shininess * 0.5 + 1.0) * pow(dotNH, shininess);
}

vec3 BRDF_BlinnPhong(in vec3 specularColor, in float shininess, in vec3 normal, in vec3 lightDir, in vec3 viewDir) {
    vec3 halfDir = normalize(lightDir + viewDir);

    float dotNH = clamp(dot(normal, halfDir), 0.0, 1.0);
    float dotLH = clamp(dot(lightDir, halfDir), 0.0, 1.0);

    vec3 F = F_Schlick(specularColor, dotLH);
    float D = D_BlinnPhong(shininess, dotNH);

    return F * 0.25 * D;
}

// Per-Pixel Tangent Space Normal Mapping
// http://hacksoflife.blogspot.ch/2009/11/per-pixel-tangent-space-normal-mapping.html

vec3 perturbNormal2Arb(vec3 eye_pos, vec3 surf_norm) {
    vec3 q0 = dFdx(eye_pos.xyz);
    vec3 q1 = dFdy(eye_pos.xyz);
    vec2 st0 = dFdx(vUv.st);
    vec2 st1 = dFdy(vUv.st);

    vec3 S = normalize(q0 * st1.t - q1 * st0.t);
    vec3 T = normalize(-q0 * st1.s + q1 * st0.s);
    vec3 N = normalize(surf_norm);

    vec3 mapN = texture2D(normalMap, vUv).xyz * 2.0 - 1.0;
    mat3 tsn = mat3(S, T, N);
    return normalize(tsn * mapN);
}

void main() {
    float specularStrength = texture2D(specularMap, vUv).r;
    vec3 normal = perturbNormal2Arb(-vViewPosition, normalize(vNormal));

    // attenuation
    float attenuation = calcLightAttenuation(length(vViewPosition.xyz), 10.0, 1.0);

    // diffuse
    vec3 lightDir = normalize(vViewPosition.xyz);
    float cosineTerm = clamp(dot(normal, lightDir), 0.0, 1.0);

    vec3 totalDiffuseLight = vec3(attenuation * cosineTerm);

    // specular
    vec3 brdf = BRDF_BlinnPhong(vec3(TWO_THIRDS, TWO_THIRDS, TWO_THIRDS), 30.0, normal, lightDir, normalize(vViewPosition));

    vec3 totalSpecularLight = brdf * specularStrength * attenuation * cosineTerm;

    vec4 diffuseColor = texture2D(map, vUv);
    vec3 outgoingLight = diffuseColor.rgb * (totalDiffuseLight + vec3(1.0, 1.0, 1.0)) + totalSpecularLight;
    gl_FragColor = vec4(outgoingLight, diffuseColor.a);
}
