/**
 * Copyright 2015-2016 by Jesse Jaara <jesse.jaara@gmail.com>
 * Copyright 2015-2016 by Jussi Karhu <karhu.jussi.t@student.uta.fi>
 *
 * This file is part of IVEditor.
 *
 * IVEditor is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * IVEditor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with IVEditor.
 * If not, see <http://www.gnu.org/licenses/>.
 *
 * @license GPL-3.0+ <http://spdx.org/licenses/GPL-3.0+>
 */

#version 100

precision mediump float;

uniform sampler2D texture;
uniform vec4 highlight;

uniform vec4 area; //[ul.x, ul.y, lr.x, lr.y]

varying vec2 vUv;

void main() {
    vec4 tpix = texture2D(texture, vUv);

    vec2 ul = area.xy;
    vec2 lr = area.zw;

    float distance = abs(lr.x - ul.x);

    if (vUv.y < ul.y && vUv.y > lr.y) {
        if (distance > 0.5) { // Highligt outside area
            if (vUv.x > ul.x || vUv.x < lr.x) {
                tpix = mix(tpix, highlight, highlight.a);
                tpix.a = 1.0;
            }
        } else {
            if (vUv.x > ul.x && vUv.x < lr.x) {
                tpix = mix(tpix, highlight, highlight.a);
                tpix.a = 1.0;
            }
        }
    }

    gl_FragColor = tpix;
}
