/**
 * Copyright 2015-2016 by Jesse Jaara <jesse.jaara@gmail.com>
 * Copyright 2015-2016 by Jussi Karhu <karhu.jussi.t@student.uta.fi>
 *
 * This file is part of IVEditor.
 *
 * IVEditor is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * IVEditor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with IVEditor.
 * If not, see <http://www.gnu.org/licenses/>.
 *
 * @license GPL-3.0+ <http://spdx.org/licenses/GPL-3.0+>
 */

#version 100

precision mediump float;

uniform mat4 projectionMatrix;
uniform mat4 modelMatrix;
uniform mat4 modelViewMatrix;
uniform vec3 cameraPosition;

uniform float mRefractionRatio;
uniform float mFresnelBias;
uniform float mFresnelScale;
uniform float mFresnelPower;
uniform float mMinZ;
uniform float mLength;
uniform float mProgress;
uniform vec3 mDefaultColor;
uniform vec3 mActivatedColor;

attribute vec3 position;
attribute vec3 normal;

varying vec3 vReflect;
varying vec3 vRefract[3];
varying float vReflectionFactor;
varying vec4 vColor;

void main()
{
// Calculate Vertex color
float p = 0.0;
if (mProgress < 0.5)
    p = clamp((distance(position.z, mMinZ))/mLength - (0.5 - mProgress), 0.0, 1.0);
else
    p = clamp((distance(position.z, mMinZ))/mLength + (mProgress - 0.5), 0.0, 1.0);
vColor.r = mDefaultColor.r * (1.0 - p) + mActivatedColor.r * p;
vColor.g = mDefaultColor.g * (1.0 - p) + mActivatedColor.g * p;
vColor.b = mDefaultColor.b * (1.0 - p) + mActivatedColor.b * p;
vColor.a = 1.0;

vec4 mvPosition = modelViewMatrix * vec4(position, 1.0);
vec4 worldPosition = modelMatrix * vec4(position, 1.0);

vec3 worldNormal = normalize(mat3(modelMatrix[0].xyz, modelMatrix[1].xyz, modelMatrix[2].xyz) * normal);

vec3 I = worldPosition.xyz - cameraPosition;

vReflect = reflect(I, worldNormal);
vRefract[0] = refract(normalize(I), worldNormal, mRefractionRatio);
vRefract[1] = refract(normalize(I), worldNormal, mRefractionRatio * 0.99);
vRefract[2] = refract(normalize(I), worldNormal, mRefractionRatio * 0.98);
vReflectionFactor = mFresnelBias + mFresnelScale * pow(1.0 + dot(normalize(I), worldNormal), mFresnelPower);

gl_Position = projectionMatrix * mvPosition;
}
