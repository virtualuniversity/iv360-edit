/* Based on Three.js's PhonMaterial shader. http://threejs.org/ */

precision mediump float;

uniform mat4 modelMatrix;
uniform mat4 modelViewMatrix;
uniform mat4 projectionMatrix;
uniform mat4 viewMatrix;
uniform mat3 normalMatrix;
uniform vec3 cameraPosition;

attribute vec3 position;
attribute vec3 normal;
attribute vec2 uv;

varying vec3 vViewPosition;
varying vec3 vNormal;
varying vec2 vUv;

void main() {
    vUv = uv;

    vNormal = normalize(normalMatrix * normal);
    vec4 mvPosition = modelViewMatrix * vec4(position, 1.0);
    vViewPosition = - mvPosition.xyz;

    gl_Position = projectionMatrix * mvPosition;
}
