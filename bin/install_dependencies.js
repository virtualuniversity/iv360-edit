#!/usr/bin/env node

/**
 * Copyright 2015-2016 by Jesse Jaara <jesse.jaara@gmail.com>
 * Copyright 2015-2016 by Jussi Karhu <karhu.jussi.t@student.uta.fi>
 *
 * This file is part of IVEditor.
 *
 * IVEditor is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * IVEditor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with IVEditor.
 * If not, see <http://www.gnu.org/licenses/>.
 *
 * @license GPL-3.0+ <http://spdx.org/licenses/GPL-3.0+>
 */

'use strict';

let buffer = require('buffer');
let Browserify = require('browserify');
let fs = require('fs-extra');
let glob = require('glob-promise');
let os = require('os');
let path = require('path');

let dependencies = [
    {
        source: 'requirejs/require.js',
        target: 'js/vendor/require.js'
    },
    {
        source: 'three/three.js',
        target: 'js/vendor/three.js'
    },
    {
        source: 'dat.gui/build/dat.gui.js',
        target: 'js/vendor/dat.gui.js'
    },
    {
        source: 'jquery/dist/jquery.js',
        target: 'js/vendor/jquery.js'
    },
    {
        source: 'jquery-ui-bundle/jquery-ui.js',
        target: 'js/vendor/jquery-ui.js'
    },
    {
        source: 'jquery-ui-bundle/jquery-ui{,.{structure,theme}}.css',
        target: 'css/'
    },
    {
        source: 'jquery-ui-bundle/images/*',
        target: 'css/images/'
    },
    {
        source: 'normalize.css/normalize.css',
        target: 'css/normalize.css'
    },
    {
        source: 'font-awesome/css/font-awesome.css',
        target: 'css/font-awesome.css',
        preprocess: fontsInAssets
    },
    { // All modern browser support this, even IE9 had support for these
        source: 'font-awesome/fonts/fontawesome-webfont.woff',
        target: 'assets/fonts/fontawesome-webfont.woff'
    },
    { // For never versions of Firefox and Chromium
        source: 'font-awesome/fonts/fontawesome-webfont.woff2',
        target: 'assets/fonts/fontawesome-webfont.woff2'
    },
    {
        source: 'markdown-it/dist/markdown-it.js',
        target: 'js/vendor/markdown-it.js'
    },
    {
        source: 'pako/dist/pako.js',
        target: 'js/vendor/pako.js'
    },
    {
        source: 'hammerjs/hammer.js',
        target: 'js/vendor/hammer.js'
    },
    {
        source: 'jszip/lib/index.js',
        target: 'js/vendor/JSZip.js',
        preprocess: compileJSZip
    }
];

function successMessage(source, target) {
    return `Successfully copied file ${source} to ${target}.`;
}

function fontsInAssets(source, target) {
    return new Promise((resolve, reject) => {
        let ofs = fs.createWriteStream(target);
        ofs.on('error', error => reject(new Error(`Failed to write FontAwesome css file. Reason: ${error}`)));
        ofs.on('finish', () => resolve(successMessage(source, target)));

        let chunks = [];
        let ifs = fs.createReadStream(source);
        ifs.on('error', error => reject(new Error(`Failed to read FontAwesome css file. Reason: ${error}`)));
        ifs.on('data', chunk => chunks.push(chunk));
        ifs.on('end', () => {
            let str = buffer.Buffer.concat(chunks).toString().replace(/fonts/g, 'assets/fonts');
            ofs.end(str);
        });
    });
}

function compileJSZip(source, target) {
    return new Promise((resolve, reject) => {
        let options = {
            detectGlobals: false,
            standalone: 'JSZip'
        };
        var bundler = new Browserify(source, options);
        bundler.external('pako');
        let bundled = bundler.bundle();

        let ofs = fs.createWriteStream(target);
        ofs.on('error', error => reject(new Error(`Failed to write FontAwesome css file. Reason: ${error}`)));
        ofs.on('finish', () => resolve(successMessage(source, target)));

        bundled.pipe(ofs);
    });
}

function copy(source, target) {
    return new Promise((resolve, reject) => {
        fs.copy(source, target, error => {
            if (error) {
                reject(`Could not copy file ${source} to ${target}.\n───Reason: ${error}`);
            } else {
                resolve(successMessage(source, target));
            }
        });
    });
}

function handleDependency(dep) {
    return new Promise((resolve, reject) => {
        glob(path.join(path.resolve('node_modules'), dep.source)).then(files => {
            let promised = [];
            let targetPath = path.resolve(dep.target);

            let targetIsDirectory = (files.length > 1);

            for (let file of files) {
                if (dep.preprocess) {
                    promised.push(dep.preprocess(file, targetPath));
                } else {
                    if (targetIsDirectory) {
                        promised.push(copy(file, path.join(targetPath, path.basename(file))));
                    } else {
                        promised.push(copy(file, targetPath));
                    }
                }
            }

            resolve(promised);
        }).catch(error => reject(`Could not build a list of source files.\n───Reason: ${error}`));
    });
}

function copyFiles(deps) {
    return new Promise((resolve, reject) => {
        let promised = [];

        for (let dep of deps) {
            promised.push(handleDependency(dep));
        }

        Promise.all(promised)
            .then(lists => resolve([].concat(...lists)))
            .catch(error => reject(error));
    });
}

function installDependencies(deps) {
    return new Promise((resolve, reject) => {
        copyFiles(deps).then(copyActions => {
            Promise.all(copyActions).then(results => {
                for (let result of results) {
                    console.log(result);
                }
                resolve(true);
            }).catch(error => reject(error));
        }).catch(error => reject(error));
    });
}

module.exports = () => installDependencies(dependencies);

// If we are running trough npm
if (require.main === module) {
    installDependencies(dependencies)
        .then(() => console.log('All frontend dependecies have been installed'))
        .catch(error => console.error(error));
}
