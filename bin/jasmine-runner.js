#!/usr/bin/env node

/**
 * Copyright 2015-2016 by Jesse Jaara <jesse.jaara@gmail.com>
 * Copyright 2015-2016 by Jussi Karhu <karhu.jussi.t@student.uta.fi>
 *
 * This file is part of IVEditor.
 *
 * IVEditor is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * IVEditor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with IVEditor.
 * If not, see <http://www.gnu.org/licenses/>.
 *
 * @license GPL-3.0+ <http://spdx.org/licenses/GPL-3.0+>
 */

'use strict';

var Jasmine = require('jasmine');
var Process = require('process');
var reporters = require('jasmine-reporters');
var _util = require('util');

var jasmine = new Jasmine();
jasmine.loadConfigFile('./tests/support/jasmine.json');

// Test if we are running the tests in Jenkins
// and output results to JUnit formatted file
if (Process.env.WORKSPACE && Process.env.BUILD_NUMBER) {
    jasmine.addReporter(new reporters.JUnitXmlReporter({
        savePath: Process.env.WORKSPACE + '/tests/results',
        filePrefix: 'VirtualUniversityTestResults-commit-' + Process.env.GIT_COMMIT
    }));
} else { // We are on local machines console, print there
    jasmine.configureDefaultReporter({showColors: true});
}

Process.on('uncaughtException',function(e) {
    _util.log('Caught unhandled exception: ' + e);
    _util.log(' ---> : ' + e.stack);
});

jasmine.execute();
