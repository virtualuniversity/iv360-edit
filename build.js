({
    baseUrl: 'js/',
    paths: {
        three: 'vendor/three'
    },
    include: ['../node_modules/almond/almond.js'],
    name: 'IVViewer',
    out: 'stdout',
    optimize: 'none',
    logLevel: 4
})
