using System;
using System.Diagnostics;

public class IVEditorLauncher {
    public static void Main() {
        if(Environment.Is64BitOperatingSystem) {
            Process.Start(@".\node64.exe", "IVEditor.js");
        } else {
            Process.Start(@".\node32.exe", "IVEditor.js");
        }
    }
}
