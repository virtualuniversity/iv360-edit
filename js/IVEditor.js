/**
 * Copyright 2015-2016 by Jesse Jaara <jesse.jaara@gmail.com>
 * Copyright 2015-2016 by Jussi Karhu <karhu.jussi.t@student.uta.fi>
 *
 * This file is part of IVEditor.
 *
 * IVEditor is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * IVEditor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with IVEditor.
 * If not, see <http://www.gnu.org/licenses/>.
 *
 * @license GPL-3.0+ <http://spdx.org/licenses/GPL-3.0+>
 */

define(['three', 'IVPlayer', 'IVProject', 'Util', 'jquery', 'jqueryui', 'dat', 'IVGlobals', 'PrettyJSON', 'EpicEditor', 'FontAwesome'],
function(Three, IVPlayer, IVProject, Util, $, jqueryui, dat, IVGlobals, PrettyJSON, EpicEditor, FontAwesome) {
    'use strict';

    // region: DatGuiAdapters

    /**
     * @param {Three.Color} color
     * @constructor
     */
    function ColorAdapter(color) {
        this.color = color;
        this._rgb = {r: color[0] * 255, g: color[1] * 255, b: color[2] * 255};
    }

    ColorAdapter.prototype.constructor = ColorAdapter;

    Object.defineProperty(ColorAdapter.prototype, 'rgb', {
        get: function() { return this._rgb; },
        set: function(value) {
            if (typeof value === 'string') {
                var tc = new Three.Color(value);
                console.log('Value:', value, 'Color:', tc);
                this._rgb.r = tc.r * 255;
                this._rgb.g = tc.g * 255;
                this._rgb.b = tc.b * 255;
            } else {
                this._rgb.r = value.r;
                this._rgb.g = value.g;
                this._rgb.b = value.b;
            }
            this.color[0] = this._rgb.r / 255;
            this.color[1] = this._rgb.g / 255;
            this.color[2] = this._rgb.b / 255;
            console.log(this.color);
        }
    });

    function ShaderAdapter(node, camera) {
        this.node = node;
        this.camera = camera;
        this._type = node.texture.fragmentShaderFile.match(/(fresnel|simple|color|phong)/i)[1].toLowerCase();
    }

    ShaderAdapter.prototype.constructor = ShaderAdapter;

    Object.defineProperty(ShaderAdapter.prototype, 'type', {
        get: function() { return this._type; },
        set: function(type) {
            var promise;
            if (type === ShaderAdapter.types.fresnel) {
                promise = IVProject.DefaultsFactory.promiseFresnel(this.camera);
            } else if (type === ShaderAdapter.types.simple) {
                promise = IVProject.DefaultsFactory.promiseSimple();
            } else if (type === ShaderAdapter.types.color) {
                promise = IVProject.DefaultsFactory.promiseColor();
            } else if (type === ShaderAdapter.types.phong) {
                promise = IVProject.DefaultsFactory.promisePhong();
            }

            promise.then(texture => {
                this._type = type;
                this.node.texture = texture;
            });
        }
    });

    ShaderAdapter.types = {
        fresnel: 'fresnel',
        simple: 'simple',
        color: 'color',
        phong: 'phong'
    };

    // endregion: DatGuiAdapters

    // region: TextNodeEditor

    /**
     * @param {IVEditor} editor
     * @constructor
     */
    function TextNodeEditor(editor) {
        this.editor = editor;
        this.dialog = $('#tnEditor').dialog({
            autoOpen: false,
            modal: true,
            minWidth: 700,
            minHeight: 500,
            open: () => this.open(),
            close: () => this.close(),
            resize: () => this.mkeditor.reflow()
        });

        this.headline = $('#headline').on('input', () => this.updateHeadline());
        this.linklist = $('#linklist');
        this.newFrameBtn = $('#newFrameBtn').on('click', () => this.addFrame());
        this.newLinkBtn = $('#newLinkBtn').on('click', () => this.addLink());

        this.tabs = $('#tabs');
    }

    TextNodeEditor.prototype.constructor = TextNodeEditor;

    /**
     * @param {IVProject.TextNode} node
     */
    TextNodeEditor.prototype.editTextNode = function(node) {
        this.node = node;
        this.dialog.dialog('open');
        this.importFrameTexts();

        if (Object.keys(node.frames).length === 0) {
            node.firstFrame = node.createFrame('New main frame', 'Write your contents here in markdown.').id;
        }

        this.tabs.empty();
        for (var frame of Object.keys(node.frames)) {
            this.createTab(node.frames[frame]);
        }
    };

    TextNodeEditor.prototype.importFrameTexts = function() {
        for (var frame of Object.keys(this.node.frames)) {
            frame = this.node.frames[frame];
            this.mkeditor.importFile(frame.id, frame.text);
        }
    };

    TextNodeEditor.prototype.close = function() {
        this.mkeditor.save(this.frame.id);
        this.frame.text = this.mkeditor.exportFile();
        this.mkeditor.unload();

        for (var frame of Object.keys(this.node.frames)) {
            for (var link of Object.keys(this.node.frames[frame].links)) {
                if (this.node.frames[frame].links[link] === 'dummy') {
                    delete this.node.frames[frame].links[link];
                }
            }
        }

        this.frame = null;
        this.node = null;
        this.currentTab = null;

        this.editor.player.play();
    };

    TextNodeEditor.prototype.open = function() {
        this.editor.player.stop();
        this.mkeditor = new EpicEditor({
            container: 'markdownEditor',
            basePath: '/css/',
            localStorageName: 'IVEditor',
            parser: mkstr => Util.TextFrame.md.render(mkstr),
            theme: {
                base: 'epiceditor.css',
                editor: 'epic-dark.css',
                preview: 'preview-dark.css'
            }
        });
        this.mkeditor.load();
    };

    TextNodeEditor.prototype.addFrame = function() {
        this.createTab(this.node.createFrame('New tab', 'Content'));
    };

    TextNodeEditor.prototype.createTab = function(frame) {
        var btn = $(`<button data-frame-id='${frame.id}' class='tntab'></button>`)
            .append($(`<span class="headlineText">${frame.headline}</span>`))
            .on('click', () => this.selectTab(btn));

        if (frame === this.node.firstFrame) {
            btn.addClass('mainFrame');
            btn.click();
        } else {
            var delBtn = $('<span class="delBtn"><i class="fa fa-trash-o"></i></span>')
                .on('click', () => this.removeTab(btn));
            btn.append(delBtn);
        }

        this.tabs.prepend(btn);
    };

    /**
     * @param {$} tab
     */
    TextNodeEditor.prototype.selectTab = function(tab) {
        if (this.currentTab) {
            this.currentTab.toggleClass('selected');
            this.mkeditor.save(this.frame.id);
            this.frame.text = this.mkeditor.exportFile();
        }

        this.currentTab = tab;
        this.currentTab.toggleClass('selected');
        this.frame = this.node.frames[this.currentTab.data('frame-id')];

        this.headline.val(this.frame.headline);

        var id = this.currentTab.data('frame-id');
        this.mkeditor.open(id);
        this.populateLinkList(id);
    };

    /**
     * @param {$} tab
     */
    TextNodeEditor.prototype.removeTab = function(tab) {
        if (tab.data('frame-id') === this.node.firstFrame.id) {
            return false;
        }
        if (this.currentTab === tab) {
            this.selectTab($(`button[data-frame-id='${this.node.firstFrame.id}']`));
        }

        var id = tab.data('frame-id');
        tab.remove();

        for (var frame of Object.keys(this.node.frames)) {
            frame = this.node.frames[frame];
            if (frame.id === id) {
                delete this.node.frames[frame.id];
            } else {
                for (var link of Object.keys(frame.links)) {
                    if (frame.links[link] === id) {
                        frame.removeLink(link);
                        break;
                    }
                }
            }
        }
    };

    TextNodeEditor.prototype.populateLinkList = function() {
        this.linklist.empty();
        for (var link of Object.keys(this.frame.links)) {
            this.createLinkItem(link, this.frame.links[link]);
        }
    };

    TextNodeEditor.prototype.createLinkItem = function(name, targetId) {
        var li = $(`<li data-target-id="${targetId}"></li>`);
        var remove = $('<button><i class="fa fa-trash-o"></i></button>')
            .on('click', () => this.removeLink(li, targetId));

        var input = $(`<input type="text" data-old-value="${name}" value="${name}">`);
        input.on('change', () => this.changeLinkName(input));

        var targets = $(`<select><option value='dummy'></option></select>`);
        targets.on('change', () => this.changeLinkTarget(input, targets));

        for (var frame of Object.keys(this.node.frames)) {
            frame = this.node.frames[frame];
            if (frame !== this.frame) {
                targets.append($(`<option value='${frame.id}'>${frame.headline}</option>`));
            }
        }

        targets.val(targetId);

        li.append(input)
            .append($('<i class="fa fa-arrow-right"></i>'))
            .append(targets)
            .append(remove);

        this.linklist.append(li);
    };

    TextNodeEditor.prototype.removeLink = function(listItem, targetId) {
        listItem.remove();
        for (var item of Object.keys(this.frame.links)) {
            if (this.frame.links[item] === targetId) {
                this.frame.removeLink(item);
                break;
            }
        }
    };

    TextNodeEditor.prototype.addLink = function() {
        this.createLinkItem('Add link text', 'dummy');
        this.frame.setLink('Add link text', 'dummy');
    };

    TextNodeEditor.prototype.changeLinkName = function(input) {
        var old = input.data('old-value');
        input.data('old-value', input.val());
        var target = this.frame.getLink(old);
        this.frame.setLink(input.val(), target);
        this.frame.removeLink(old);
    };

    TextNodeEditor.prototype.changeLinkTarget = function(input, targets) {
        this.frame.setLink(input.val(), targets.val());
    };

    TextNodeEditor.prototype.updateHeadline = function() {
        this.currentTab.children('.headlineText').text(this.headline.val());
        this.frame.headline = this.headline.val();
    };

    // endregion: TextNodeEditor

    // region: IVEditorController

    /**
     * @param {IVEditor} editor
     * @constructor
     */
    function IVEditorController(editor) {
        this.editor = editor;
        this.bindEventListeners();
    }

    /** Function sets up a set of other functions to listen on specific events for
     * the given elements.
     */
    IVEditorController.prototype.bindEventListeners = function() {
        var container = this.editor.player.container;

        container.addEventListener(
            'dblclick', event => this.onContainerDblClick(event), false);

        document.getElementById('newSceneBtn').addEventListener(
            'click', event => this.editor.newScene(event), false);

        document.getElementById('loadBtn').addEventListener('click', () => this.editor.openBrowser());

        document.getElementById('previewBtn').addEventListener('click', () => this.editor.previewProject());

        document.getElementById('exportBtn').addEventListener('click', () => this.editor.saveProject());

        document.getElementById('zipBtn').addEventListener('click', () => this.editor.zipProject());

        window.addEventListener('resize', () => {
            $('#projectList').css('max-height', $(window).height() - 250);
        });
    };

    /** When user is interacting by doubleclicking with mouse, this function checks if the click
     * intersects with an object in 3D space at the position of mouse cursor. If intersection occures,
     * it either creates a new contentNode or selects clicked contentNode to the editor.
     *
     * @param {MouseEvent} event - Mouse event.
     */
    IVEditorController.prototype.onContainerDblClick = function(event) {
        var mouse = new Three.Vector2();
        var raycaster = new Three.Raycaster();
        var intersect;

        mouse.x = (event.clientX / window.innerWidth) * 2 - 1; // x
        mouse.y = -(event.clientY / window.innerHeight) * 2 + 1; // y

        raycaster.setFromCamera(mouse, this.editor.player.camera);
        var intersects = raycaster.intersectObjects(this.editor.player.scene.children);

        if (intersects.length > 0) {
            intersect = intersects[ 0 ];
            if (intersect.object.uuid === this.editor.player.scene.children[0].uuid) {
                this.editor.createContentNode(intersect.point);
            } else {
                this.editor.selectNode(this.editor.getNodeByMeshId(intersect.object.uuid));
            }
        }
    };

    // endregion: IVEditorController

    // region: IVEditor

    /**
     * @param {IVPlayer.IVPlayer} player
     * @constructor
     */
    function IVEditor(player) {
        this.player = player;
        this.player.activateNodes = false;

        this.initEditor();
    }

    /** Function creates new project.
     * @returns {Promise}
     */
    IVEditor.prototype.newProject = function() {
        return new Promise((resolve) => {
            IVProject.DefaultsFactory.promiseDome().then(dome => {
                var project = new IVProject.Project();
                var newScene = new IVProject.Scene({skydome: dome, name: 'Scene 0'});
                project.scenes.mainScene = newScene;
                resolve(project);
            }).catch(reason => console.error(reason));
        });
    };

    /**
     * Load an existing project or create a new one
     * if one does not exist.
     *
     * @returns {Promise}
     */
    IVEditor.prototype.getProject = function() {
        return new Promise((resolve, reject) => {
            Util.get('/rest/project/json').then(response => {
                var json = JSON.parse(response.file);
                var cam = this.player.cubecam.renderTarget;
                IVProject.Project.fromJSON(json, cam)
                    .then(project => resolve(project))
                    .catch(reason => reject(reason));
            }).catch(reason => {
                console.log(reason);
                if (typeof reason !== 'string') {
                    this.newProject()
                        .then(project => resolve(project))
                        .catch(reason => reject(reason));
                }
                // TODO: Handle server errors
            });
        });
    };
    /**
     * Opens file browser to open a project.
     */
    IVEditor.prototype.openBrowser = function() {
        if (!this.saved) {
            if (confirm('You have unsaved changes in the current project. Are you sure you want to open a new one?')) {
                window.location.assign('../browser');
            }
        } else {
            window.location.assign('../browser');
        }

    };

    /**
     * Opens project in viewer
     */
    IVEditor.prototype.previewProject = function() {
        if (!this.saved) {
            if (confirm('You have unsaved changes in the current project. Would you like to save them first so you can preview them too?')) {
                this.saveProject();
                window.location.assign('/html/IVViewer.html');
            } else {
                window.location.assign('/html/IVViewer.html');
            }
        } else {
            window.location.assign('/html/IVViewer.html');
        }
    };

    /** Function creates new scene to current project.
     *
     */
    IVEditor.prototype.newScene = function() {
        IVProject.DefaultsFactory.promiseDome().then(dome => {
            var i = this.project.scenes.size;
            var newScene = new IVProject.Scene({skydome: dome, name: 'Scene ' + i});
            this.project.scenes.set(newScene.id, newScene);
            this.scene = this.project.scenes.get(newScene.id);
            this.selectNode(this.scene);
            this.saved = false;
        }).catch(reason => console.error(reason));
    };

    /** Function initialises editor.
     *
     */
    IVEditor.prototype.initEditor = function() {
        this.getProject().then(project => {
            this.saved = true;
            this.project = project;
            this.controls = {};
            this.showHighlight = false;
            $('#editor').draggable({
                containment: '#container',
                stop: function(event, ui) {
                    ui.helper.css({'height': 'auto'});
                }
            });
            $('#projectList').css('max-height', $(window).height() - 250);
            this.selectNode(this.project.mainScene);
            this.player.animate(performance.now());
            this.tnEditor = new TextNodeEditor(this);
        }).catch(reason => console.log(reason))
        .then(() => {
            $('.hidden').removeClass('hidden');
            $('#spinner').addClass('hidden');
        });
    };

    IVEditor.prototype.saveProject = function() {
        var files = this.project.getFiles();
        console.log("I'm going to send the following assets to the server:", this.project.getFiles());
        Util.post('/rest/project/assets/save', JSON.stringify({files: files})).then(fileStatus => {
            var data = PrettyJSON.stringify(this.project, {indent: 2, maxLength: 120});
            Util.post('/rest/project/json', JSON.stringify({data: data}))
                .then(status => window.alert(fileStatus.message + '\n' + status.message))
                .catch(status => window.alert(status.message));
        }).catch(status => window.alert(status.message));
        this.saved = true;
    };

    IVEditor.prototype.zipProject = function() {
        Util.get('/rest/project/zip').then(response => {
            alert(response.message);
        }).catch(response => alert(response.message));
    };

    IVEditor.prototype.updateProjectList = function() {
        $('#editor > h3').remove();
        $('#projectList').empty();
        this.addProjectToList();
    };

    /** Function creates new ContentNode with given coordinates and
     *  selects that node to editor.
     * @param {Three.Vector3} position - Position for new ContentNode.
     */
    IVEditor.prototype.createContentNode = function(position) {
        IVProject.DefaultsFactory.promiseArrow(IVProject.GatewayNode, this.player.cubecam.renderTarget).then(node => {
            var pos = Util.SPCoordinate.fromCartesian(position);
            pos.radius = IVGlobals.SCALE * 0.8;
            node.position = pos;
            node.activationBox = new Util.SPBox(5, 5, 5, 5);
            node.name = 'ContentNode ' + this.scene.nodes.size;
            this.scene.addContentNode(node);
            this.selectNode(node);
            this.saved = false;
        });
    };

    IVEditor.prototype.createPositionControls = function() {
        this.controls.pos = this.gui.addFolder('Position');

        this.controls.pos.radius = this.controls.pos.add(this.cur._position, 'radius', 0, 10);
        this.controls.pos.radius.onChange(() => this.updatePosition());

        this.controls.pos.zenith = this.controls.pos.add(this.cur._position, 'zenith', -90, 90);
        this.controls.pos.zenith.onChange(() => this.updatePosition());

        this.controls.pos.azimuth = this.controls.pos.add(this.cur._position, 'azimuth', 0, 360);
        this.controls.pos.azimuth.onChange(() => this.updatePosition());

        this.controls.pos.open();
    };

    IVEditor.prototype.createRotationControls = function() {
        this.controls.rot = this.gui.addFolder('Rotation');

        for (var axis of ['x', 'y', 'z']) {
            this.controls.rot[axis] = this.controls.rot.add(this.cur.mesh.rotation, axis, 0, IVGlobals.TWO_PI).listen();
            this.controls.rot[axis].onChange(() => this.updateRotation());
        }

        this.controls.rot.open();
    };

    IVEditor.prototype.createGatewayNodeControl = function() {
        var targets = {};
        for (var scene of this.project.scenes) {
            scene = scene[1];
            if (scene !== this.player.ivscene) {
                targets[scene.name] = scene.id;
            }
        }
        this.controls.specific = this.gui.addFolder('Gateway Node');
        this.controls.target = this.controls.specific.add(this.cur, 'target').options(targets).name('Gateway target');
        this.controls.target.onChange(() => this.updateTarget());
        this.controls.specific.open();
    };

    IVEditor.prototype.createTextNodeControl = function() {
        this.controls.specific = this.gui.addFolder('Text Node');
        this.controls.edittext = this.controls.specific.add(this, 'editTextNode').name('Edit Text Node');
        this.controls.specific.open();
    };

    IVEditor.prototype.createAudioNodeControl = function() {
        this.controls.audio = this.gui.addFolder('Audio Node');
        this.controls.file = this.controls.audio.add(this.cur, 'file').name('Audio file').options(this.assets.audios);
        this.controls.volume = this.controls.audio.add(this.cur, '_volume', 0.0, 1).name('Volume');
        this.controls.autoplay = this.controls.audio.add(this.cur, 'autoplay').name('Autoplay');
        this.controls.loop = this.controls.audio.add(this.cur, '_loop').name('Loop');
        this.controls.audio.open();
    };

    IVEditor.prototype.createActivationAreaControls = function() {
        this.controls.area = this.gui.addFolder('Activation Area');

        this.controls.area.highlight = this.controls.area.add(this.player, 'showHighlight').name('Show highlight');
        this.controls.area.highlight.onChange(() => this.updateActivationArea());

        var box = this.cur.activationBox;
        this.controls.area.box = box;

        this.controls.area.up = this.controls.area.add(box, 'up', 0, 90);
        this.controls.area.up.onChange(() => this.updateActivationArea());

        this.controls.area.down = this.controls.area.add(box, 'down', 0, 90);
        this.controls.area.down.onChange(() => this.updateActivationArea());

        this.controls.area.left = this.controls.area.add(box, 'left', 0, 90);
        this.controls.area.left.onChange(() => this.updateActivationArea());

        this.controls.area.right = this.controls.area.add(box, 'right', 0, 90);
        this.controls.area.right.onChange(() => this.updateActivationArea());
    };

    IVEditor.prototype.createTypeSpecificControls = function() {
        if (this.cur instanceof IVProject.GatewayNode) {
            this.createGatewayNodeControl();
        } else if (this.cur instanceof IVProject.TextNode) {
            this.createTextNodeControl();
        } else if (this.cur instanceof IVProject.AudioNode) {
            this.createAudioNodeControl();
        }
    };

    IVEditor.prototype.createProjectControls = function() {
        this.controls.project = this.gui.addFolder('Project');

        this.controls.project.location = this.controls.project.add(this.project, 'location');
        this.controls.project.copyright = this.controls.project.add(this.project, 'copyright');
        this.controls.project.author = this.controls.project.add(this.project, 'author');

        this.controls.project.open();
    };

    IVEditor.prototype.createTextureControls = function() {
        var tc = this.gui.addFolder('Texture');

        tc.type = tc.add(new ShaderAdapter(this.cur, this.player.cubecam.renderTarget), 'type', ShaderAdapter.types);
        tc.type.onChange(() => {
            this.updateControls();
        });

        for (var pair of this.cur.texture.uniforms) {
            var name = pair[0];
            var uniform = pair[1];

            if (uniform.type === 'f') {
                tc[name] = tc.add(uniform, 'value').name(name);
            } else if (uniform.type === 'c') {
                tc[name] = tc.addColor(new ColorAdapter(uniform.value), 'rgb').name(name);
            } else if (uniform.type.match('v')) {
                tc[name] = tc.addFolder(name);
                tc[name].x = tc[name].add(uniform.value, 'x');
                tc[name].y = tc[name].add(uniform.value, 'y');

                if (uniform.type !== 'v2') {
                    tc[name].z = tc[name].add(uniform.value, 'z');
                    if (uniform.type !== 'v3') {
                        tc[name].w = tc[name].add(uniform.value, 'w');
                    }
                }
            } else if (uniform.type === 'img') {
                tc[name] = tc.add(uniform, 'value', this.assets.textures).name(name);
            }
            // TODO: Implement matrixes somehow

            if (tc[name]) {
                tc[name].onChange(() => this.cur.texture.refreshUniforms());
            }
        }

        this.controls.texture = tc;
    };

    /** Function updates editor to display given ContentNode.
     *
     */
    IVEditor.prototype.updateControls = function() {
        if (this.gui) {
            this.gui.destroy();
        }

        IVEditor.getAssetlist().then(assets => {
            this.assets = assets;
            this.gui = new dat.GUI({width: 300, resizable: false});
            this.controls.name = this.gui.add(this.cur, 'name').name('Name');
            this.controls.name.onChange(() => {
                this.updateProjectList();
                this.saved = false;
            });
            this.controls.description = this.gui.add(this.cur, 'description').name('Description');
            this.controls.description.onChange(() => {
                this.saved = false;
            });

            if (this.cur instanceof IVProject.Scene) {
                this.controls.skydome = this.gui.add(this.cur.skydome, 'fileURL', assets.skydomes).name('Skydome');
                this.controls.skydome.onChange(url => this.updateSkydome(url));
            } else if (this.cur instanceof IVProject.Project) {
                this.createProjectControls();
            } else if (this.cur instanceof IVProject.ContentNode) {

                this.controls.type = this.gui.add(this.cur, 'type', ['static', 'textual', 'gateway', 'audio']).name('Type');
                this.controls.type.onChange(value => this.updateType(value));

                this.createTypeSpecificControls();

                this.controls.model = this.gui.add(this.cur, 'modelFile').options(assets.models).name('Model');
                this.controls.model.onChange(() => this.updateModel());

                this.createTextureControls();

                this.createPositionControls();
                this.createRotationControls();

                this.controls.scale = this.gui.add(this.cur.scale, 'x', 0.1, 2).name('Scale');
                this.controls.scale.onChange(value => this.cur.setScale(new Three.Vector3(value, value, value)));

                this.createActivationAreaControls();
                this.controls.time = this.gui.add(this.cur, 'activationTime', 1, 10).name('Activation time');
                this.controls.time.onChange(() => {
                    this.saved = false;
                });

            }
        });
    };

    /**
     * Function updates the skydome of current scene.
     *
     * @param {String} url
     */
    IVEditor.prototype.updateSkydome = function(url) {
        var filetype = url.substr(url.lastIndexOf('.') + 1);
        if (filetype === 'mp4') {
            this.scene.skydome.setVideo(url);
            $('#videocontrols').show();
        } else {
            this.scene.skydome.setImage(url);
            $('#videocontrols').hide();
        }
        this.player.setScene(this.scene);
    };

    IVEditor.prototype.updateActivationArea = function() {
        this.cur.activationBox = this.controls.area.box;
        this.player.highlightArea(this.cur.activationArea.ul, this.cur.activationArea.lr);
        this.saved = false;
    };

    /** Function updates the type of current content node.
     *
     * @param {String} type
     */
    IVEditor.prototype.updateType = function(type) {
        var id = this.cur.id;
        this.scene.changeNodeToType(this.cur, type);
        this.cur = this.scene.nodes.get(id);
        this.updateControls();
        this.saved = false;
    };

    /**
     * Function updates the target of current gateway node.
     */
    IVEditor.prototype.updateTarget = function() {
        this.cur.target = this.controls.target.getValue();
        this.saved = false;
    };

    /**
     * Function updates position of current content node.
     */
    IVEditor.prototype.updatePosition = function() {
        this.cur.position = new Util.SPCoordinate(
            this.controls.pos.radius.getValue(),
            this.controls.pos.zenith.getValue(),
            this.controls.pos.azimuth.getValue()
        );
        this.updateActivationArea();
    };

    /**
     * Function updates rotation of current content node.
     */
    IVEditor.prototype.updateRotation = function() {
        this.cur.setRotation(new Three.Euler(
            this.controls.rot.x.getValue(),
            this.controls.rot.y.getValue(),
            this.controls.rot.z.getValue(),
            'XYZ'
        ));
        this.saved = false;
    };


    IVEditor.getAssetlist = function() {
        return new Promise((resolve, reject)=> {
            Util.get('/rest/assets/list').then(assets => {
                var assetlist = assets.program;

                for (var type of Object.keys(assetlist)) {
                    for (var file of Object.keys(assetlist[type])) {
                        assetlist[type][file] = Util.AssetsLoader.assetsProtocol + assetlist[type][file];
                    }
                }

                assetlist.models['Default Arrow'] = 'Default arrow';
                assetlist.models['Default Speaker'] = 'Default speaker';
                assetlist.models['Font Awesome Symbol'] = 'fa-symbol';

                for (var type of Object.keys(assets.project)) {
                    for (var file of Object.keys(assets.project[type])) {
                        assetlist[type][file] = Util.AssetsLoader.projectProtocol + assets.project[type][file];
                    }
                }

                resolve(assetlist);
            }).catch(reason => reject(reason));
        });
    };

    IVEditor.promiseSymbol = function() {
        return new Promise(resolve => {
            var input = $("<input style='font-family: FontAwesome' type='text'>");

            var dialog = $('<div></div>')
                .append($('<p>Please copy a symbol from Font Awesome website to the box below.</p>'))
                .append(input)
                .dialog({
                    appendTo: 'body',
                    dialogClass: "icondialog",
                    buttons: {
                        OK: function() {
                            resolve(generateSymbol(input.val(), {}));
                            $(this).dialog('close');
                        }
                    }
                });
        });
    };

    IVEditor.prototype.updateModelOnScene = function(node) {
        this.player.scene.remove(node.oldMesh);
        this.player.scene.add(node.mesh);
    };

    /**
     * Function updates model and texture of current content node.
     */
    IVEditor.prototype.updateModel = function() {
        var node = this.cur;
        var p;
        if (node.modelFile === 'Default arrow') {
            p = IVProject.DefaultsFactory.promiseArrow(node, this.player.cubecam.renderTarget);
        } else if (node.modelFile === 'Default speaker') {
            p = IVProject.DefaultsFactory.promiseSpeaker(node);
        } else if (node.modelFile === 'fa-symbol') {
            p = new Promise((resolve, reject) => {
                IVEditor.promiseSymbol().then(geometry => {
                    node.setModel(geometry);
                    resolve(true);
                }).catch(reason => reject(reason));
            });
        } else {
            p = new Promise((resolve, reject) => {
                Util.AssetsLoader.promise(node.modelFile).then(geometry => {
                    node.setModel(geometry);
                    resolve(true);
                }).catch(reason => reject(reason));
            });
        }

        p.then(() => this.updateModelOnScene(node)).catch(reason => console.error(reason));

        this.saved = false;
    };

    /** Function adds project to the list.
     *
     * @param {project} -  project
     */
    IVEditor.prototype.addProjectToList = function() {
         var project = this.project;
         var $proj = $('<h3>' + project.name + '</h3>');
         $proj.on('click', {editor: this}, this.selectProject);
         $('#editor').prepend($proj);

         var $list = $('#projectList');
         //var $proj = this.createListItem(project);
         var $div = $('<div class="dg"></div>');
         var $sdiv = $('<div class="dg scene"></div>');
         var $ndiv;
         var $scenes = $('<ul></ul>');
         project.scenes.forEach(scene => {
             var $scene = this.createListItem(scene);
             $scene.addClass('scene');
             if (scene.nodes.size > 0) {
                 $scene.addClass('foldable');
             }
             $scene.children().first().addClass('header');
             var $nodes = $('<ul></ul>');
             scene.nodes.forEach(node => {
                 var $node = this.createListItem(node);
                 $node.addClass('node');
                 $nodes.append($node);
             });
             $ndiv = $('<div class="dg node"></div>');
             $ndiv.append($nodes);
             $scene.append($ndiv);
             $scenes.append($scene);
         });
         $sdiv.append($scenes);
         $div.append($sdiv);
         $list.append($div);

         $('#projectList li[data-id="' + this.cur.id + '"]').addClass('selected');
         $('#projectList li[data-id="' + this.scene.id + '"]').addClass('open');
     };

    /** Function adds new Content node list in editor to display all created ContentNodes.
     *
     */
    IVEditor.prototype.createListItem = function(listItem) {
        var $li = $('<li class="cr"></li>');
        var icon;

        if (listItem instanceof IVProject.Scene) {
            icon = '<i class="fa fa-globe listicon sceneicon"></i>';
        } else if (listItem instanceof IVProject.ContentNode) {
            icon = '<i class="fa fa-map-pin listicon nodeicon"></i>';
        }

        var $div = $('<div>' + '<span>' + icon + listItem.name + '</span></div>');
        $div.on('click', {editor: this}, this.selectNodeFromList);
        var $delBtn = $('<span class="delBtn"><i class="fa fa-trash-o"></i></span>');
        $delBtn.on('click', {editor: this}, this.removeNodeFromList);
        $div.append($delBtn);
        $li.append($div);
        $li.attr('data-id', listItem.id);
        return $li;
    };

    /** Function updates editor to display selected ContentNode.
     *
     */
    IVEditor.prototype.selectNodeFromList = function(event) {
        var selected = $(event.currentTarget).parent();
        var nodeId = selected.data('id');
        event.data.editor.selectNode(event.data.editor.getById(nodeId));
        event.stopPropagation();
        event.preventDefault();
    };

    /** Function updates editor to display the project.
     *
     */
    IVEditor.prototype.selectProject = function(event) {
        event.data.editor.selectNode(event.data.editor.project);
        event.stopPropagation();
        event.preventDefault();
    };

    /** Function removes selected node.
     *
     */
    IVEditor.prototype.removeNodeFromList = function(event) {
        var editor = event.data.editor;
        var selected = $(event.currentTarget).parents('li');
        var nodeId = selected.data('id');
        var node = editor.getById(nodeId);
        var parent = editor.getParentById(nodeId);
        var cur = false;

        if (node instanceof IVProject.Scene) {
            if (node === editor.scene) {
                cur = true;
            }
            if (node.nodes.size > 0) {
                if (confirm('This scene contains ' + node.nodes.size + ' content nodes.' +
                'They will also be deleted and cannot be recovered. Are you sure you want to delete it?')) {
                    parent.scenes.delete(nodeId);
                    if (cur) {
                        if (editor.project.scenes.size > 0) {
                            editor.selectNode(editor.project.scenes.values().next().value);
                        } else {
                            editor.newScene();
                        }
                    } else {
                        editor.updateProjectList();
                    }
                }
            }
        } else if (node instanceof IVProject.ContentNode) {
            if (node === editor.cur) {
                cur = true;
            }
            parent.nodes.delete(nodeId);
            editor.player.scene.remove(node.mesh);
            if (cur) {
                if (parent.nodes.size > 0) {
                    editor.selectNode(parent.nodes.values().next().value);
                } else {
                    editor.selectNode(parent);
                }
            } else {
                editor.updateProjectList();
            }
        }
        editor.saved = false;
        event.stopPropagation();
        event.preventDefault();
    };

    /** Function updates editor to display selected ContentNode and adds
     *  class "selected" to that node's list item.
     *
     *  @param {number} selectedNum - Index number of selected contentNode
     */
    IVEditor.prototype.selectNode = function(node) {
        if (!(this.cur instanceof IVProject.ContentNode)) {
            this.player.showHighlight = false;
        }
        this.cur = node;
        if (node instanceof IVProject.Scene) {
            this.scene = node;
            if (this.scene !== this.player.ivscene) {
                if (this.scene.skydome.type === IVProject.Skydome.Type.VIDEO) {
                    $('#videocontrols').show();
                } else {
                    $('#videocontrols').hide();
                }
                this.player.setScene(this.scene);
            }
        } else if (node instanceof IVProject.ContentNode) {
            this.scene = this.getParentById(node.id);
            if (this.scene !== this.player.ivscene) {
                this.player.setScene(this.scene);
            }
            this.player.highlightArea(this.cur.activationArea.ul, this.cur.activationArea.lr);
            this.player.lookAt(this.cur.position);
        }

        this.updateControls();
        this.updateProjectList();
    };

    IVEditor.prototype.editTextNode = function() {
        if (this.cur instanceof IVProject.TextNode) {
            this.tnEditor.editTextNode(this.cur);
        } else {
            alert('Currently selected node is not a text node.');
        }
        this.saved = false;
    };

    /**
    * Function finds and returns object (scene or content node) by ID.
    *
    * @param {String} id
    */
    IVEditor.prototype.getById = function(id) {
        var retval;
        var project = this.project;
        var notfound = true;
        var found = project.scenes.get(id);
        if (typeof found !== 'undefined' && notfound) {
            retval = found;
            notfound = false;
        }else {
            project.scenes.forEach(scene => {
                found = scene.nodes.get(id);
                if (typeof found !== 'undefined' && notfound) {
                    retval = found;
                    notfound = false;
                }
            });
        }
        return retval;
    };

    /**
    * Function finds and returns object's parent (project or scene) by ID.
    *
    * @param {String} id
    */
    IVEditor.prototype.getParentById = function(id) {
        var parent;
        var project = this.project;
        var notfound = true;
        var object = project.scenes.get(id);
        if (typeof object !== 'undefined' && notfound) {
            parent = project;
            notfound = false;
        }else {
            project.scenes.forEach(scene => {
                object = scene.nodes.get(id);
                if (typeof object !== 'undefined' && notfound) {
                    parent = scene;
                    notfound = false;
                }
            });
        }
        return parent;
    };

    /**
    * Function finds and returns content node by mesh ID.
    *
    * @param {String} id
    */
    IVEditor.prototype.getNodeByMeshId = function(id) {
        var retval;
        this.scene.nodes.forEach(node => {
            if (node.mesh.uuid === id) {
                retval = node;
            }
        });
        return retval;
    };

    // endregion: IVEditor

    // region: Helper functions

    function generateSymbol(symbol, parameters) {
        if (symbol.length > 1) {
            symbol = String.fromCodePoint('0x' + symbol);
        }

        parameters.font = FontAwesome;
        parameters.size = parameters.size || 1;
        parameters.height = parameters.height || 0.5;
        parameters.curveSegments = 12;

        var geometry = new Three.TextGeometry(symbol, parameters);
        geometry.normalize();
        geometry.center();

        return geometry;
    }

    // endregion: Helper functions

    return {
        IVEditor: IVEditor,
        IVEditorController: IVEditorController
    };
});
