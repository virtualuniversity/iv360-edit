/**
 * Copyright 2015-2016 by Jesse Jaara <jesse.jaara@gmail.com>
 * Copyright 2015-2016 by Jussi Karhu <karhu.jussi.t@student.uta.fi>
 *
 * This file is part of IVEditor.
 *
 * IVEditor is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * IVEditor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with IVEditor.
 * If not, see <http://www.gnu.org/licenses/>.
 *
 * @license GPL-3.0+ <http://spdx.org/licenses/GPL-3.0+>
 */

define(function() {
    'use strict';

    // We need the empty dummy to make testing work for testing.
    var _AudioContext = function() {};
    if (typeof window !== 'undefined') {
        _AudioContext = window.AudioContext || window.webkitAudioContext;
    }

    var fmtvers = {
        major: 0,
        minor: 10,
        patch: 0,
        toJSON: function() { return `${this.major}.${this.minor}.${this.patch}`; }
    };

    return {
        SCALE: 10,
        AudioContext: new _AudioContext(),
        PI_PER_180: 0.01745329251,
        RAD_IN_DEG: 57.295779513,
        TWO_PI: 6.28318530718,
        ExportFileFormatVersion: fmtvers
    };
});
