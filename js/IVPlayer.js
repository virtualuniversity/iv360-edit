/**
 * Copyright 2015-2016 by Jesse Jaara <jesse.jaara@gmail.com>
 * Copyright 2015-2016 by Jussi Karhu <karhu.jussi.t@student.uta.fi>
 *
 * This file is part of IVEditor.
 *
 * IVEditor is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * IVEditor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with IVEditor.
 * If not, see <http://www.gnu.org/licenses/>.
 *
 * @license GPL-3.0+ <http://spdx.org/licenses/GPL-3.0+>
 */

/**
 * 360 Player
 * <p>
 * This is player for 360 -degrees video.
 * <p>
 * @author Jesse Jaara
 * @author Jussi Karhu
 * @author Santeri Taskinen
 * @author Sami Voutilainen
 *
 * @return {Object} EXPORT - Export is the returnable objects reference.
 */

/** Declaration of PLAYER object variable point to function. This function declarates variables
  * needed on this file and specific 'object': EXPORT.
  */
define(['three', 'IVProject', 'Util', 'IVGlobals', 'Hammer', 'StereoEffect'],
function(Three, IVProject, Util, IVGlobals, Hammer, StereoEffect) {
    'use strict';

    // Declaration of globals and introducing default values
    var MAX_ZOOM = 75;
    var MIN_ZOOM = 20;

    var SPHERE_WH_SEGMENTS = 64;

    class IVPlayerViewerController {
        constructor(player) {
            this.player = player;
            this.findElements();

            this.lookAt = new Util.SPCoordinate(1, 0, 0);
            this.player.activateFrame = node => this.activateTextNode(node);
            this.closeButton.addEventListener('click', event => this.toggleFrameholder(event), false);

            this.useOrientation = false;

            this.setupMouseControls();
            this.setupTouchControls();
        }

        /**
         * @param {MouseEvent} event
         */
        static pointerLockPan(event) {
            if (!this.useOrientation) {
                if (!this.controller.mozPrefixed) {
                    this.controller.lookAt.zenith += event.movementY * 0.5;
                    this.controller.lookAt.azimuth += event.movementX * 0.5;
                } else {
                    this.controller.lookAt.zenith += event.mozMovementY * 0.5;
                    this.controller.lookAt.azimuth += event.mozMovementX * 0.5;
                }

                this.player.lookAt(this.controller.lookAt);
            }
        }

        static get HIDDEN_CLASS() { return 'hidden'; }

        static createPointerLocklistener(controller) {
            return {
                handleEvent: IVPlayerViewerController.pointerLockPan,
                player: controller.player,
                controller: controller
            };
        }

        setupTouchControls() {
            this.lastDelta = {};
            this.hammer = new Hammer(this.player.container);
            this.hammer.get('pan').set({direction: Hammer.DIRECTION_ALL});
            this.hammer.on('panmove', event => this.touchPan(event));
            this.hammer.on('panstart', () => {
                this.lastDelta.x = 0;
                this.lastDelta.y = 0;
            });
        }

        /**
         * @param {Hammer.Event} event
         */
        touchPan(event) {
            if (!this.useOrientation) {
                this.lookAt.azimuth += (this.lastDelta.x - event.deltaX) * 0.3;
                this.lookAt.zenith += (this.lastDelta.y - event.deltaY) * -0.3;
                this.player.lookAt(this.lookAt);

                this.lastDelta.x = event.deltaX;
                this.lastDelta.y = event.deltaY;
            }
        }

        setupMouseControls() {
            this.detectPointerLock();

            if (this.pointerLockable) {
                this.pointerLockListener = IVPlayerViewerController.createPointerLocklistener(this);
                this.setupPointerLockListener();
            } else {
                this.player.container.addEventListener('mousemove', event => this.simplePan(event), false);
            }
        }

        createQuaternion(alpha, beta, gamma) {
            let finalQuaternion = new Three.Quaternion();
            finalQuaternion.setFromEuler(new Three.Euler(beta, alpha, -gamma, 'YXZ'));
            finalQuaternion.multiply(this.screenTransform);
            finalQuaternion.multiply(this.worldTransform);

            return finalQuaternion;
        }

        onDeviceOrientationChange(event) {
            if (this.player.paused || !this.useOrientation) {
                return;
            }

            let alpha = Three.Math.degToRad(event.alpha); // Z
            let beta = Three.Math.degToRad(event.beta); // X'
            let gamma = Three.Math.degToRad(event.gamma); // Y''

            let deviceQuat = this.createQuaternion(alpha, beta, gamma);
            //this.object.quaternion.slerp(deviceQuat, 0.07); // smoothing

            let direction = new Three.Vector3(0, 0, -1);
            direction.applyQuaternion(deviceQuat);
            this.player.lookTarget = Util.SPCoordinate.fromCartesian(direction);
            this.player.camera.quaternion.copy(deviceQuat);
            this.player.updateAudioListener();
        }

        updateScreenOrientation() {
            let minusHalfAngle = -Three.Math.degToRad(window.orientation) / 2;
            this.screenTransform = new Three.Quaternion(0, Math.sin(minusHalfAngle), 0, Math.cos(minusHalfAngle));
        }

        setupOrientationControls() {
            this.useOrientation = true;
            this.worldTransform = new Three.Quaternion(-Math.sqrt(0.5), 0, 0, Math.sqrt(0.5));
            window.addEventListener('deviceorientation', e => this.onDeviceOrientationChange(e), false);
            window.addEventListener('orientationchange', () => this.updateScreenOrientation(), false);
            this.updateScreenOrientation();
        }

        findElements() {
            this.frameholder = document.getElementById('frameholder');
            this.frameHeader = document.getElementById('frameheader');
            this.mainSection = document.getElementById('mainSection');
            this.buttonBox = document.getElementById('buttonbox');
            this.closeButton = document.getElementById('closebtn');
        }

        detectPointerLock() {
            this.pointerLockable = (typeof this.player.container.requestPointerLock === 'function');
            if (!this.pointerLockable && typeof this.player.container.mozRequestPointerLock === 'function') {
                this.mozPrefixed = true;
                this.pointerLockable = true;
            } else {
                this.mozPrefixed = false;
            }
        }

        toggleFrameholder() {
            if (this.player.paused) {
                this.player.play();
            } else {
                this.player.stop();
            }

            this.frameholder.classList.toggle(IVPlayerViewerController.HIDDEN_CLASS);
        }

        setTextFrame(frame) {
            Util.clearElement(this.frameHeader);
            Util.clearElement(this.mainSection);
            Util.clearElement(this.buttonBox);

            frame.render((name, target) => this.linkHandler(name, target));
            this.frameHeader.appendChild(frame.h1);
            this.frameHeader.appendChild(this.closeButton);
            this.mainSection.appendChild(frame.section);
            this.buttonBox.appendChild(frame.buttons);
        }

        activateTextNode(node) {
            this.textNode = node;
            this.setTextFrame(node.firstFrame);
            document.exitPointerLock = document.exitPointerLock || document.mozExitPointerLock;
            document.exitPointerLock();
            this.toggleFrameholder();
        }

        linkHandler(name, target) {
            this.setTextFrame(this.textNode.getFrame(target));
        }

        /**
         * @param {MouseEvent} event
         */
        simplePan(event) {
            if (!this.useOrientation) {
                this.lookAt.zenith = (event.clientY - (window.innerHeight / 2)) / window.innerHeight * 180;
                this.lookAt.azimuth = (event.clientX - (window.innerWidth / 2)) / window.innerWidth * 360;
                this.player.lookAt(this.lookAt);
            }
        }

        setupPointerLockListener() {
            this.player.container.addEventListener('mousedown', event => this.lockPointer(event), false);

            if (!this.mozPrefixed) {
                document.addEventListener('pointerlockchange', event => this.toggleLocking(event), false);
            } else {
                document.addEventListener('mozpointerlockchange', event => this.toggleLocking(event), false);
            }
        }

        lockPointer() {
            if (!this.mozPrefixed) {
                this.player.container.requestPointerLock();
            } else {
                this.player.container.mozRequestPointerLock();
            }
        }

        toggleLocking() {
            let pointerLockElement = document.pointerLockElement || document.mozPointerLockElement;

            if (pointerLockElement !== this.player.container) {
                this.player.container.removeEventListener('mousemove', this.pointerLockListener);
            } else if (pointerLockElement === this.player.container) {
                this.player.container.addEventListener('mousemove', this.pointerLockListener, false);
            }
        }
    }

    class IVPlayerController {
        /**
         * @param {IVPlayer} player
         * @constructor
         */
        constructor(player) {
            this.player = player;

            this.seekBar = document.getElementById('seekBar');
            this.ctime = document.getElementById('ctime');
            this.ttime = document.getElementById('ttime');
            this.playPauseBtn = document.getElementById('play-pause-btn');
            this.muteBtn = document.getElementById('mute-btn');

            this.zoomSpeed = 0.15;
            this.spanSpeed = 0.1;

            this.userIsInteracting = false;
            this.mouseDownPoint = new Three.Vector2(0.0, 0.0);
            this.mouseDownSPC = new Util.SPCoordinate(1, 0, 0);

            this.bindEventListeners();
        }

        /** Function sets up a set of other functions to listen on specific events for
         * the given elements. These elements are mouse pushed, mouse moving, stop pushing
         * and making changes to mouse wheel position.
         */
        bindEventListeners() {
            let container = this.player.container;
            container.addEventListener('mousedown', event => this.onMouseDown(event), false);
            container.addEventListener('mousemove', event => this.onMouseMove(event), false);
            container.addEventListener('mouseup', event => this.onMouseUp(event), false);
            container.addEventListener('wheel', event => this.onMouseWheel(event), false);

            this.player.video.addEventListener('loadedmetadata', event => this.onLoadedMetadata(event), false);
            this.player.video.addEventListener('timeupdate', event => this.updateProgress(event), false);

            this.seekBar.addEventListener('change', event => this.seekVideo(event), false);

            this.playPauseBtn.addEventListener('click', event => this.togglePlayPause(event), false);
            this.muteBtn.addEventListener('click', event => this.toggleMute(event), false);
        }

        /** While user is interacting by pushing mouse down, this function changes the events
         * coordinate values from mouse push starting point. This happens at mouse down.
         *
         * @param {MouseEvent} event - Event what user or browser does.
         */
        onMouseDown(event) {
            event.preventDefault();

            if (event.button === 0) {
                this.userIsInteracting = true;

                this.mouseDownPoint.set(event.clientX, event.clientY);
                this.mouseDownSPC = this.player.lookTarget;
            }
        }

        /** When user is moving mouse and mouse is pressed, it will affect to coordinate
         * values lon and lat. Function calculates those values from mouse movements.
         *
         * @param {MouseEvent} event - Event what user or browser does.
         */
        onMouseMove(event) {
            if (this.userIsInteracting === true) {
                let yChange = event.clientY - this.mouseDownPoint.y;
                let xChange = this.mouseDownPoint.x - event.clientX;
                let zenith = yChange * this.spanSpeed + this.mouseDownSPC.zenith;
                let azimuth = xChange * this.spanSpeed + this.mouseDownSPC.azimuth;

                this.player.lookAt(new Util.SPCoordinate(1, zenith, azimuth));
            }
        }

        /** When user is not anymore pushing the mouse button, this function turn boolean
         * interaction flag to false. It tells if user is pressing and moving the mouse.
         *
         */
        onMouseUp() {
            this.userIsInteracting = false;
        }

        /** When user is interacting with mouse wheel, it will affect to cameras field of
         * view(fov) and projection matrix.
         *
         * @param {WheelEvent} event - Event what user od browser does.
         */
        onMouseWheel(event) {
            this.player.alterZoom(event.deltaY * this.zoomSpeed);
        }

        /** Function gives basic video controls to user. Function sniffs if
         * the video is paused, ended or playing and makes changes to variables.
         *
         * @param {MouseEvent} event
         */
        togglePlayPause() {
            let btn = this.playPauseBtn;

            if (this.player.video.paused || this.player.video.ended) {
                btn.title = 'pause';
                btn.firstChild.classList.remove('fa-play');
                btn.firstChild.classList.add('fa-pause');
                this.player.video.play();
            } else {
                btn.title = 'play';
                btn.firstChild.classList.remove('fa-pause');
                btn.firstChild.classList.add('fa-play');
                this.player.video.pause();
            }
        }

        /** Function tracks if mute button is pressed down or if is not.
         * Function makes changes to certain values.
         *
         * @param {MouseEvent} event
         */
        toggleMute() {
            let btn = this.muteBtn;

            if (this.player.video.muted) {
                btn.title = 'mute';
                btn.firstChild.classList.remove('fa-volume-off');
                btn.firstChild.classList.add('fa-volume-up');
                this.player.video.muted = false;
            } else {
                btn.title = 'unmute';
                btn.firstChild.classList.remove('fa-volume-up');
                btn.firstChild.classList.add('fa-volume-off');
                this.player.video.muted = true;
            }
        }

        /** Function controls the seekBar of the video. It updates the video progess
         * to be seen in movement of bar and 'ctime'.
         */
        seekVideo() {
            this.player.video.currentTime = this.player.video.duration * (this.seekBar.value / 100);
        }

        /** Function controls the seekBar of the video. It updates the video progess
         * to be seen in movement of bar and 'ctime'.
         */
        updateProgress() {
            this.seekBar.value = this.player.video.currentTime * (100 / this.player.video.duration);
            this.ctime.innerHTML = IVPlayerController.formatTime(this.player.video.currentTime);
        }

        onLoadedMetadata() {
            this.ttime.innerHTML = IVPlayerController.formatTime(this.player.video.duration);
        }

        /** Function formats time from seconds to minutes and seconds and
         * returns it as a string.
         *
         * @param {Number} sec - Time in seconds
         * @return {String} - Time in formatted as minutes:seconds
         */
        static formatTime(sec) {
            let min;
            min = Math.floor(sec / 60);
            min = (min >= 10) ? min : '0' + min;
            sec = Math.floor(sec % 60);
            sec = (sec >= 10) ? sec : '0' + sec;
            return min + ':' + sec;
        }
    }

    class IVPlayer {
        /**
         /** This is the 360 players initialization function. Function run trough declaration of objects
         * needed in this player. It also give basic values for objects. Function also calls other
         * functions to introduce specific values for certain objects. It calls for renderer function,
         * for pixels and for window size. It also calls listener for user mouse movements and interaction.
         *
         * @param {HTMLElement} container - Specific container from the HTML file, which contains whole window.
         * @param {Number} scale - Scaling of the sphere and scene
         * @constructor
         */
        constructor(container) {
            this.video = IVPlayer.prepareVideoElement();
            this.videoTexture = new Three.VideoTexture(this.video);
            this.videoTexture.minFilter = Three.LinearFilter;

            this.scale = IVGlobals.SCALE;

            this.audioCtx = IVGlobals.AudioContext;
            this.listener = this.audioCtx.listener;
            this.listener.setPosition(0, 0, 0);

            this.project = null;

            this.oldStamp = 0;
            this.cubecam = new Three.CubeCamera(0.1, 100, 256);

            this.camera = new Three.PerspectiveCamera(75, window.innerWidth / window.innerHeight, 1, 1100);
            this.lookAt(new Util.SPCoordinate(IVGlobals.SCALE, 0, 0));

            this.scene = new Three.Scene();

            this.renderer = new Three.WebGLRenderer({antialias: true});
            this.renderer.setPixelRatio(window.devicePixelRatio);
            this.renderer.setSize(window.innerWidth, window.innerHeight);
            this.rendererOnUse = this.renderer;

            this.stereo = new StereoEffect(this.renderer);

            this.container = container;
            this.container.appendChild(this.renderer.domElement);

            this.paused = false;
            this.pausedDueToHide = false;
            this.activateNodes = true;

            window.addEventListener('resize', event => this.onWindowResize(event), false);
            document.addEventListener('visibilitychange', event => this.pauseOnHide(event), false);
        }

        /**
         * Create a new player in a safe manner that makes sure the player is
         * fully loaded before allowing playback.
         *
         * @param {HTMLElement} container - Specific container from the HTML file, which contains whole window.
         * @returns {Promise<IVPlayer>}
         */
        static init(container) {
            let player = new IVPlayer(container);

            return new Promise(resolve => {
                player.sphere = IVPlayer.prepareSphere().then(sphere => {
                    player.sphereMaterial = sphere.material;
                    player.sphere = sphere;
                    player.scene.add(player.sphere);
                    resolve(player);
                });
            });
        }

        static prepareVideoElement() {
            let video = document.createElement('video');
            video.autoplay = true;
            video.loop = true;
            video.crossOrigin = 'anonymous';

            return video;
        }

        /** Function prepares sphere or 360 bubble to its default values and returns
         * it to calling function.
         *
         * @return {Promise<Three.Mesh>} geometry - Is the 360 bubble.
         */
        static prepareSphere() {
            return new Promise(resolve => {
                let geometry = new Three.SphereGeometry(IVGlobals.SCALE, SPHERE_WH_SEGMENTS, SPHERE_WH_SEGMENTS);
                geometry.scale(-1, 1, 1); // Flip the thing inside out, so that texture maps correctly

                let fragURL = Util.AssetsLoader.assetsProtocol + '/assets/shaders/fragment/VideoHighlight-fragment.glsl';
                let vertexURL = Util.AssetsLoader.assetsProtocol + '/assets/shaders/vertex/Simple-vertex.glsl';
                let textureURL = Util.AssetsLoader.assetsProtocol + '/assets/textures/CheckerBoard.png';

                let promised = [
                    Util.AssetsLoader.promise(fragURL),
                    Util.AssetsLoader.promise(vertexURL),
                    Util.AssetsLoader.promise(textureURL)
                ];

                Promise.all(promised).then(assets => {
                    let material = new Three.RawShaderMaterial({
                        fragmentShader: assets[0],
                        vertexShader: assets[1],
                        uniforms: {
                            texture: {type: 't', value: assets[2]},
                            area: {type: 'v4', value: new Three.Vector4(0, 0, 0, 0)},
                            highlight: {type: 'v4', value: new Three.Vector4(1, 0, 0, 0.5)}
                        }
                    });

                    resolve(new Three.Mesh(geometry, material));
                });
            });
        }

        pauseOnHide() {
            if (document.hidden && !this.video.paused) {
                this.video.pause();
                this.audioCtx.suspend();
                this.pausedDueToHide = true;
            } else if (!document.hidden && this.pausedDueToHide) {
                this.video.play();
                this.audioCtx.resume();
                this.pausedDueToHide = false;
            }
        }

        stop() {
            this.video.pause();
            this.audioCtx.suspend();
            this.paused = true;
        }

        play() {
            this.video.play();
            this.audioCtx.resume();
            this.oldStamp = performance.now();
            this.paused = false;
            this.animate(performance.now());
        }

        /**
         * Load the project and set up the player for it.
         *
         * @returns {Promise<IVProject.Project>}
         */
        loadProject(file) {
            return new Promise((resolve, reject) => {
                let loader = new XMLHttpRequest();
                loader.onload = () => {
                    IVProject.Project.fromJSON(JSON.parse(loader.responseText), this.cubecam.renderTarget).then(project => {
                        this.project = project;
                        this.setupNodeCallbacks();

                        this.setScene(this.project.mainScene);
                        this.stop();
                        resolve(this.project);
                    }).catch(reason => reject(`Could not project. Reason: ${reason}`));
                };
                loader.open('GET', file);
                loader.send();
            });
        }

        setupNodeCallbacks() {
            let callBack = node => this.setScene(this.project.sceneByID(node.target));
            let linkHandler = node => this.activateFrame(node);

            for (let scene of this.project.scenes) {
                for (let node of scene[1].nodes) {
                    if (node[1] instanceof IVProject.GatewayNode) {
                        node[1].callback = callBack;
                    } else if (node[1] instanceof IVProject.TextNode) {
                        node[1].callback = linkHandler;
                    }
                }
            }
        }

        /**
         * @param {IVProject.Scene} ivscene
         */
        setScene(ivscene) {
            if (this.ivscene) {
                for (let pair of this.ivscene.nodes) {
                    if (pair[1] instanceof IVProject.AudioNode) {
                        pair[1].stop();
                    }
                }
            }

            this.scene = new Three.Scene();
            this.scene.add(this.sphere);
            this.ivscene = ivscene;
            if (this.ivscene) {
                this.video.pause();

                ivscene.nodes.forEach(node => {
                    node.resetTicker();
                    this.scene.add(node.mesh);

                    if (node instanceof IVProject.AudioNode) {
                        node.deactivated = false;
                    }
                });

                if (ivscene.skydome.type === IVProject.Skydome.Type.VIDEO) {
                    this.setVideo();
                } else if (ivscene.skydome.type === IVProject.Skydome.Type.IMAGE) {
                    this.setImage();
                }
            }
        }

        setVideo() {
            this.video.src = Util.AssetsLoader.getRealUrl(this.ivscene.skydome.fileURL);
            this.sphere.material = this.sphereMaterial;
            this.sphere.material.uniforms.texture.value = this.videoTexture;
            this.sphere.material.needsUpdate = true;
        }

        setImage() {
            Util.AssetsLoader.promise(this.ivscene.skydome.fileURL).then(img => {
                this.sphere.material = this.sphereMaterial;
                this.sphere.material.uniforms.texture.value = img;
                this.sphere.material.needsUpdate = true;
            });
        }

        /**
         * Alter the cameras field of view, that is the level of zooming.
         *
         * A negative value decreases the zoom level and a positive one increases it.
         *
         * @param {Number} level - A delta value
         */
        alterZoom(level) {
            this.camera.fov = Three.Math.clamp(this.camera.fov - level, MIN_ZOOM, MAX_ZOOM);
            this.camera.updateProjectionMatrix();
        }

        /**
         * @param {Three.Vector3 | Util.SPCoordinate} point
         */
        lookAt(point) {
            if (point instanceof Three.Vector3) {
                this.lookTarget = Util.SPCoordinate.fromCartesian(point);
                this.lookTarget.radius = IVGlobals.SCALE;
            } else {
                this.lookTarget = point;
            }

            this.camera.lookAt(this.lookTarget.toCartesian());
        }

        updateAudioListener() {
            let front = this.lookTarget.frontVector();
            let up = this.camera.up;
            this.listener.setOrientation(front.x, front.y, front.z, up.x, up.y, up.z);
        }

        /** When user resize the player window, aspect ratio and renderer values changes.
         * This function starts changes when this happens.
         *
         */
        onWindowResize() {
            this.camera.aspect = window.innerWidth / window.innerHeight;
            this.camera.updateProjectionMatrix();

            this.renderer.setSize(window.innerWidth, window.innerHeight);
        }

        /**
         * Highlight the area between ul (upper left corner) and lr (lower right corner).
         *
         * @param {Util.SPCoordinate} ul
         * @param {Util.SPCoordinate} lr
         */
        highlightArea(ul, lr) {
            let area = new Three.Vector4(
                ul.azimuth * 0.002777777777777778, // 1/360
                (ul.zenith + 90) * 0.005555555555555556, // 1/180
                lr.azimuth * 0.002777777777777778,
                (lr.zenith + 90) * 0.005555555555555556
            );

            this.sphere.material.uniforms.area.value.copy(area);
        }

        /** @param {Boolean} state */
        set showHighlight(state) {
            if (state === true) {
                this.sphere.material.uniforms.highlight.value.w = 0.5;
            } else {
                this.sphere.material.uniforms.highlight.value.w = 0.0;
            }
        }

        get showHighlight() {
            return this.sphere.material.uniforms.highlight.value.w > 0;
        }

        toggleHighlight(state) {
            if (state === true) {
                this.sphere.material.uniforms.highlight.value.w = 0.5;
            } else {
                this.sphere.material.uniforms.highlight.value.w = 0.0;
            }
        }

        /** Function updates the animated (or video) to next 360 picture and calls for
         * updating function.
         *
         */
        animate(stamp) {
            if (!this.paused) {
                requestAnimationFrame(this.animate.bind(this));
                this.update(stamp);
            }
        }

        /** Function updates / repaints the 360 picture to sphere and updates the
         * camera target position.
         *
         */
        update(stamp) {
            let ticks = stamp - this.oldStamp;
            this.oldStamp = stamp;

            this.renderContentNodes(ticks);

            this.cubecam.updateCubeMap(this.renderer, this.scene);
            this.rendererOnUse.render(this.scene, this.camera);
        }

        /**
         * @param {Number} ticks
         */
        renderContentNodes(ticks) {
            this.ivscene.nodes.forEach(node => {
                node.updateUniforms(this.video.duration, this.video.currentTime);
                if (this.activateNodes) {
                    node.tick(this.lookTarget, ticks);
                }
            });
        }
    }

    return {
        IVPlayer: IVPlayer,
        IVPlayerController: IVPlayerController,
        IVPlayerViewerController: IVPlayerViewerController
    };
});
