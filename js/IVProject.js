/**
 * Copyright 2015-2016 by Jesse Jaara <jesse.jaara@gmail.com>
 * Copyright 2015-2016 by Jussi Karhu <karhu.jussi.t@student.uta.fi>
 *
 * This file is part of IVEditor.
 *
 * IVEditor is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * IVEditor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with IVEditor.
 * If not, see <http://www.gnu.org/licenses/>.
 *
 * @license GPL-3.0+ <http://spdx.org/licenses/GPL-3.0+>
 */

define(['three', 'Util', 'IVGlobals', 'OBJExporter'],
function(Three, Util, IVGlobals, OBJExporter) {
    'use strict';

    class Texture {
        /**
         * Texture
         *
         * @param {{
         * vertexShaderFile: String,
         * fragmentShaderFile: String,
         * uniforms: Array<Object>,
         * threejsTexture: Three.Material,
         * camera: Three.CubeCamera}} options
         *
         * @constructor
         * @extends {IVComponent}
         */
        constructor(options) {
            options = options || {};

            this.vertexShaderFile = options.vertexShaderFile || '';
            this.fragmentShaderFile = options.fragmentShaderFile || '';
            this.uniforms = options.uniforms || {};
            this.threejsTexture = new Three.RawShaderMaterial({
                vertexShader: Texture.defaultVertextShader,
                fragmentShader: Texture.defaultFragmentShader
            });
            this.camera = options.camera || null;
        }

        /**
         * @param {String} vertexUrl
         * @param {String} fragmentUrl
         * @param {Object} uniforms
         * @param {Three.Camera} camera
         * @param {String} filePrefix
         * @returns {Promise<Texture>}
         */
        static loadFromAssets(vertexUrl, fragmentUrl, uniforms, camera, filePrefix) {
            return new Promise((resolve, reject) => {
                filePrefix = filePrefix || '';
                let tex = new Texture({
                    camera: camera,
                    uniforms: new Util.Uniforms(Util.Uniforms.parseJSON(uniforms, filePrefix))
                });

                let inLoading = [
                    tex.loadShader(filePrefix + vertexUrl, 'VERTEX'),
                    tex.loadShader(filePrefix + fragmentUrl, 'FRAGMENT'),
                    tex.uniforms.toThreeJS(camera)];

                Promise.all(inLoading).then(resolved => {
                    tex.threejsTexture.uniforms = resolved[2];
                    tex.threejsTexture.needsUpdate = true;

                    resolve(tex);
                }).catch(reason => reject(new Error(`Failed to load needed assets for Texture reason: ${reason}`)));
            });
        }

        /**
         * @param {Object} json
         * @param {Three.Camera} camera
         * @returns {Promise<Texture>}
         */
        static fromJSON(json, camera) {
            console.log(`Loading texture`);
            return Texture.loadFromAssets(json.vertexShader,
                json.fragmentShader,
                json.uniforms,
                camera,
                Util.AssetsLoader.projectProtocol);
        }

        static get defaultVertextShader() {
            return '#version 100\n' +
                'uniform mat4 projectionMatrix;\n' +
                'uniform mat4 modelViewMatrix;\n' +
                'attribute vec3 position;\n' +
                'void main() {\n' +
                '    gl_Position = projectionMatrix * modelViewMatrix * vec4( position, 1.0 );\n' +
                '}';
        }

        static get defaultFragmentShader() {
            return '#version 100\n' +
                'void main() {\n' +
                'gl_FragColor = vec4( 1.0, 0.0, 0.0, 1.0 );\n' +
                '}';
        }

        toJSON() {
            return {
                vertexShader: Util.AssetsLoader.stripProtocol(this.vertexShaderFile),
                fragmentShader: Util.AssetsLoader.stripProtocol(this.fragmentShaderFile),
                uniforms: this.uniforms
            };
        }

        getFiles() {
            let files = [];

            if (Util.AssetsLoader.internalAsset(this.vertexShaderFile)) {
                files.push(Util.AssetsLoader.getInternalAsset(this.vertexShaderFile));
            }

            if (Util.AssetsLoader.internalAsset(this.fragmentShaderFile)) {
                files.push(Util.AssetsLoader.getInternalAsset(this.fragmentShaderFile));
            }

            return files.concat(this.uniforms.getFiles());
        }

        /**
         * Load and set the file pointed by the given URL as the vertex or fragment shader.
         *
         * @param {String} url
         * @param {String} type - 'VERTEX' or 'FRAGMENT'
         * @returns {Promise<String>} resolves to the loaded shader
         */
        loadShader(url, type) {
            return new Promise((resolve, reject) => {
                Util.AssetsLoader.promise(url).then(shader => {
                    if (type === 'VERTEX') {
                        this.vertexShaderFile = url;
                        this.threejsTexture.vertexShader = shader;
                    } else if (type === 'FRAGMENT') {
                        this.fragmentShaderFile = url;
                        this.threejsTexture.fragmentShader = shader;
                    }

                    resolve(shader);
                }).catch(reason => reject(new Error(`
                }Failed to load ${type} shader from file ${url}: reason ${reason}`)));
            });
        }

        refreshUniforms() {
            this.uniforms.updated = true;
            this.uniforms.toThreeJS().then(result => {
                this.threejsTexture.uniforms = result;
                this.threejsTexture.__webglShader.uniforms = result;
                this.threejsTexture.needsUpdate = true;
            });
        }
    }

    class IVComponent {
        /**
         * IVComponent
         *
         * @param {{
         * id: String,
         * name: String,
         * description: String}} options
         *
         * @constructor
         */
        constructor(options) {
            // We do not want to change the ID, as that would mean walking down the
            // project tree and updating all references.
            Object.defineProperty(this, 'id', {
                configurable: false,
                enumerable: true,
                value: options.id || Three.Math.generateUUID(),
                writable: false
            });
            this.name = options.name || this.id;
            this.description = options.description || '';
        }

        /**
         * @param {Object} json
         * @returns {{id: String, name: String, description: String}}
         */
        static toOptions(json) {
            return {
                id: json.id,
                name: json.name,
                description: json.description
            };
        }
    }

    class ContentNode extends IVComponent {
        /**
         * ContentNode
         *
         * @param {{
         * name: String,
         * description: String,
         * model: Three.Geometry,
         * texture: Texture,
         * position: Util.SPCoordinate,
         * scale: Three.Vector3,
         * rotation: Three.Euler,
         * activationTime: Number,
         * callBack: Function}} options
         *
         * @constructor
         * @extends {IVComponent}
         */
        constructor(options) {
            options = options || {};
            super(options);

            this.type = 'static';
            this._texture = options.texture || new Texture();
            this._position = options.position || new Util.SPCoordinate(IVGlobals.SCALE, 0, 0);
            this.modelFile = options.modelFile || null;
            this.rotation = options.rotation || new Three.Euler();
            this.scale = options.scale || new Three.Vector3(1, 1, 1);

            this.mesh = new Three.Mesh(options.model || new Three.BoxGeometry(1, 1, 1), this.texture.threejsTexture);
            this.mesh.position.copy(this.position.toCartesian());
            this.mesh.scale.copy(this.scale);
            this.mesh.rotation.copy(this.rotation);
            if (options.activationArea) {
                this.activationArea = options.activationArea;
            } else {
                this.activationBox = new Util.SPBox();
            }

            this.callback = options.callback || function () {
                };
            this._activationTime = options.activationTime || 1000;
            this.ticker = new Util.TriggeringCounter(this._activationTime, () => {
                this.callback(this);
            });

            this.oldMesh = options.oldMesh || this.mesh;
        }

        toJSON() {
            let json = {
                id: this.id,
                name: this.name,
                description: this.description,
                position: this.position,
                scale: Util.vectorToJSON(this.scale),
                rotation: Util.eulerToJSON(this.rotation),
                activationTime: this.activationTime,
                activationArea: {ul: this.activationArea.ul.toJSON(), lr: this.activationArea.lr.toJSON()},
                model: (this.modelFile) ? Util.AssetsLoader.stripProtocol(this.modelFile) : null,
                texture: (this.modelFile) ? this.texture : null,
                type: 'static'
            };
            json.activationArea.ul[0] = 1;
            json.activationArea.lr[0] = 1;

            return json;
        }

        getFiles() {
            let files = [];

            if (Util.AssetsLoader.internalAsset(this.modelFile)) {
                files.push(Util.AssetsLoader.getInternalAsset(this.modelFile));
            } else if (this.mesh.geometry instanceof Three.TextGeometry) {
                // TODO: Add support for non FontAwesome text. Data and params can be encoded in comments in the obj file
                let params = this.mesh.geometry.parameters;
                let codepoint = this.mesh.geometry.text.codePointAt(0).toString(16);
                let filename = `fas_${codepoint}_${params.size}_${params.height}_${params.size}_${params.curveSegments}`;
                if (params.bevelEnabled) {
                    filename += `_${params.bevelThickness}_${params.bevelSize}`;
                }
                filename = '/assets/models/' + filename + '.obj';
                this.modelFile = filename;

                files.push({
                    action: 'save',
                    to: filename,
                    data: OBJExporter.parse(this.mesh)
                });
            }

            return files.concat(this.texture.getFiles());
        }

        set activationTime(value) {
            this._activationTime = value * 1000;
            this.ticker.to = this._activationTime;
        }

        get activationTime() { return this._activationTime / 1000; }

        /** @param {Util.SPCoordinate} value */
        set position(value) {
            this._position = value;
            this.mesh.position.copy(this._position.toCartesian());
        }
        /** @returns {Util.SPCoordinate} */
        get position() { return this._position; }

        get texture() { return this._texture; }
        set texture(texture) {
            this._texture = texture;
            this.mesh.material = texture.threejsTexture;
            this.mesh.needsUpdate = true;
        }

        get activationBox() {
            return Util.SPBox.fromCorners(this.position, this.activationArea.ul, this.activationArea.lr);
        }

        set activationBox(box) { this.activationArea = box.cornersForPoint(this.position); }

        /**
         * @param {String} corner
         * @param {(Util.SPCoordinate | Three.Vector3)} coordinate
         */
        setActivationAreaCorner(corner, coordinate) {
            let c = coordinate;
            if (coordinate instanceof Three.Vector3) {
                c = Util.SPCoordinate.fromCartesian(coordinate);
            }

            if (corner === 'ul') {
                this.activationArea.ul = c;
            } else if (corner === 'lr') {
                this.activationArea.lr = c;
            }
        }

        /**
         * @param {(Util.SPCoordinate | Three.Vector3)} coordinate
         */
        setPosition(coordinate) {
            let pos = coordinate;
            if (coordinate instanceof Three.Vector3) {
                pos = Util.SPCoordinate.fromCartesian(coordinate);
            }

            this.position = pos;
            this.mesh.position.copy(coordinate);
        }

        /**
         * @param {Three.Euler} rotation
         */
        setRotation(rotation) {
            this.rotation = rotation;
            this.mesh.rotation.copy(rotation);
        }

        /**
         * @param {Three.Vector3} scale
         */
        setScale(scale) {
            this.scale = scale;
            this.mesh.scale.copy(scale);
        }

        /**
         * @param {Three.Geometry} model
         */
        setModel(model) {
            this.oldMesh = this.mesh;
            this.model = model;
            this.mesh = new Three.Mesh(model, this.texture.threejsTexture);
            this.mesh.position.copy(this.position.toCartesian());
            this.mesh.rotation.copy(this.rotation);
            this.mesh.scale.copy(this.scale);
        }

        /**
         * @param {Util.SPCoordinate} point
         * @return {Boolean}
         */
        pointInActivationArea(point) {
            return point.between(this.activationArea.ul, this.activationArea.lr);
        }

        /**
         * @param {Util.SPCoordinate} point
         * @param {Number} ticks number of ticks to tick
         */
        tick(point, ticks) {
            if (this.pointInActivationArea(point)) {
                this.ticker.tick(ticks);
                console.log(`Node ${this.name} in focus.`);
            } else {
                this.ticker.tick(-ticks);
            }
        }

        resetTicker() {
            this.ticker.value = 0;
        }

        updateUniforms(videoLenght, videoPosition) {
            this.texture.threejsTexture.uniforms.mVideoLength = {type: 'f', value: videoLenght};
            this.texture.threejsTexture.uniforms.mVideoPosition = {type: 'f', value: videoPosition};
            let mProgress = this.ticker.value / this.ticker.to;
            this.texture.threejsTexture.uniforms.mProgress = {type: 'f', value: mProgress};
            this.texture.threejsTexture.needsUpdate = true;
        }
    }

    class GatewayNode extends ContentNode {
        /**
         * @param {{
         * name: String,
         * description: String,
         * model: Three.Geometry,
         * texture: Texture,
         * position: Util.SPCoordinate,
         * scale: Three.Vector3,
         * rotation: Three.Euler,
         * target: String}} options
         *
         * @constructor
         */
        constructor(options) {
            options = options || {};

            super(options);

            this.type = 'gateway';
            this.target = options.target || this.id;
        }

        toJSON() {
            let cnode = super.toJSON(this);
            cnode.target = this.target;
            cnode.type = 'gateway';

            return cnode;
        }
    }

    class IVAudioNode extends ContentNode {
        /**
         * Internally called IVAudioNode to prevent name clash with Web Audio API's
         * AudioNode interface. Exported class is name AudioNode as it can can/will
         * be referred to within a scope.
         *
         * @param {{
         * name: String,
         * description: String,
         * position: Util.SPCoordinate,
         * file: String,
         * volume: Number,
         * autoPlay: Boolean,
         * loop: Boolean,
         * activationTime: Number,
         * activationArea: {ul: Util.SPCoordinate, lr: Util.SPCoordinate}}} options
         *
         * @constructor
         */
        constructor(options) {
            options = options || {};
            options.scale = options.scale || new Three.Vector3(1, 1, 1);
            options.rotation = options.rotation || new Three.Euler(0, 0, 0);

            super(options);

            this.type = 'audio';
            this._volume = options.volume || 1;
            this.autoplay = options.autoplay || false;
            this._loop = options.loop || false;

            if (options.file) {
                this.file = options.file;
            } else {
                this._file = '';
            }

            this.WANode = null;

            this.callback = function () {
                this.WANode.play();
            };
        }

        static fromJSON(json, options) {
            return new Promise((resolve, reject) => {
                console.log(`Loading AudionNode ${options.name}`);

                options.volume = json.volume;
                options.autoplay = json.autoplay;
                options.loop = json.loop;

                let node = new IVAudioNode(options);
                node.setFile(Util.AssetsLoader.projectProtocol + json.file).then(() => {
                    resolve(node);
                }).catch(reason => reject(reason));
            });
        }

        autoplayTick() {
            if (!this.deactivated) {
                this.WANode.play();
                this.deactivated = !this._loop;
            }
        }

        toJSON() {
            let cnode = super.toJSON(this);
            cnode.volume = this.volume;
            cnode.loop = this.loop;
            cnode.autoplay = this.autoplay;
            cnode.file = Util.AssetsLoader.stripProtocol(this.file);
            cnode.type = 'audio';

            return cnode;
        }

        getFiles() {
            let files = super.getFiles();

            if (Util.AssetsLoader.internalAsset(this.file)) {
                files.push(Util.AssetsLoader.getInternalAsset(this.file));
            }

            return files;
        }

        stop() {
            this.WANode.stop();
            this.deactivated = true;
        }

        setFile(file) {
            return new Promise((resolve, reject) => {
                this._file = file;
                Util.AssetsLoader.promise(file).then(buffer => {
                    this.WANode = new Util.AutomatedAudioNode(IVGlobals.AudioContext, buffer, this.position);
                    this.WANode.gain = this.volume;
                    this.WANode.loop = this.loop;
                    resolve(this.WANode);
                }).catch(reason => reject(reason));
            });
        }

        set file(value) { this.setFile(value).catch(reason => console.log(reason)); }
        get file() { return this._file; }

        set volume(value) {
            this._volume = value;
            this.WANode.gain = value;
        }
        get volume() { return this._volume; }

        set loop(value) {
            this._loop = value;
            this.WANode.loop = value;
        }
        get loop() { return this._loop; }

        set autoplay(value) {
            this._autoplay = value;
            if (value) {
                this.tick = this.autoplayTick;
            } else {
                this.tick = ContentNode.prototype.tick;
            }
        }
        get autoplay() { return this._autoplay; }
    }

    class TextNode extends ContentNode {
        /**
         *
         * @param {{
         * name: String,
         * description: String,
         * position: Util.SPCoordinate,
         * activationTime: Number,
         * activationArea: {ul: Util.SPCoordinate, lr: Util.SPCoordinate},
         * frames: {},
         * firstFrame: String}} options
         *
         * @constructor
         */
        constructor(options) {
            options = options || {};
            super(options);

            this.type = 'textual';
            this.frames = options.frames || {};
            this.firstFrame = options.firstFrame;
        }

        /**
         * Generates a standard previous frame button for the given frame.
         *
         * @param {String} frameID
         * @param {{}} frames
         */
        static setUpPreviusLinks(frameID, frames) {
            let frame = frames[frameID];
            for (let link in frame.links) {
                if (link !== Util.TextFrame.PREVIOUS_LINK) {
                    let child = frames[frame.getLink(link)];
                    child.setLink(Util.TextFrame.PREVIOUS_LINK, frameID);
                }
            }
        }

        /**
         * @param {Object<String, *>} json
         * @param {Object<String, *>} options
         */
        static fromJSON(json, options) {
            return new Promise((resolve, reject) => {
                console.log(`Loading TextNode ${options.name}`);
                options.firstFrame = json.firstFrame;
                options.frames = {};

                for (let frame of json.frames) {
                    options.frames[frame.id] = Util.TextFrame.fromJSON(frame);
                }

                for (let frame in options.frames) {
                    TextNode.setUpPreviusLinks(frame, options.frames);
                }

                resolve(new TextNode(options));
            });
        }

        set firstFrame(id) { this._firstFrame = id; }
        get firstFrame() { return this.frames[this._firstFrame]; }

        /**
         * @returns {Object}
         */
        toJSON() {
            let cnode = super.toJSON(this);
            cnode.type = 'textual';
            cnode.firstFrame = this.firstFrame.id;
            cnode.frames = [];

            for (let frame of Object.keys(this.frames)) {
                cnode.frames.push(this.frames[frame]);
            }

            return cnode;
        }

        /**
         * @param {String} id
         * @returns {Util.TextFrame}
         */
        getFrame(id) { return this.frames[id]; }

        /**
         * Creates a new TextFrame into the TextNode and returns it for editing.
         *
         * @param {String | undefined | null} headline
         * @param {String | undefined | null} text
         * @param {(Object | undefined | null)} links
         * @returns {Util.TextFrame}
         */
        createFrame(headline, text, links) {
            let frame = new Util.TextFrame({headline: headline || '', text: text || '', links: links || {}});
            this.frames[frame.id] = frame;
            return frame;
        }
    }

    class ContentNodeFactory {
        /**
         * Creates a new ContentNode with the specified subtype.
         *
         * @param {String} type
         * @param {Object<String, *>} options
         * @returns {ContentNode}
         */
        static create(type, options) {
            switch (type) {
                case 'static':
                    return new ContentNode(options);
                case 'textual':
                    return new TextNode(options);
                case 'gateway':
                    return new GatewayNode(options);
                case 'audio':
                    return new IVAudioNode(options);
            }
        }

        /**
         * Converts the given ContentNode sub type to another node type.
         *
         * @param {String} type - reference to the class object
         * @param {ContentNode} old
         * @returns {ContentNode}
         */
        static convert(type, old) {
            let options = {
                id: old.id,
                name: old.name,
                description: old.description,
                position: old.position,
                scale: old.scale,
                modelFile: old.modelFile,
                rotation: old.rotation,
                model: old.mesh.geometry,
                texture: old.texture,
                activationTime: old._activationTime,
                activationArea: old.activationArea,
                oldMesh: old.mesh
            };

            return ContentNodeFactory.create(type, options);
        }

        /**
         * @param {Object<String, *>} json
         * @param {Three.Camera} camera
         * @returns {Promise<ContentNode>}
         */
        static fromJSON(json, camera) {
            let options = {
                modelFile: (json.model !== null) ? Util.AssetsLoader.projectProtocol + json.model : null,
                texture: null, // Will be loaded in the promise
                position: new Util.SPCoordinate(json.position[0] * IVGlobals.SCALE, json.position[1], json.position[2]),
                rotation: Util.JSONToEuler(json.rotation),
                scale: new Three.Vector3(json.scale[0], json.scale[1], json.scale[2]),
                activationArea: {
                    ul: Util.SPCoordinate.fromArray(json.activationArea.ul),
                    lr: Util.SPCoordinate.fromArray(json.activationArea.lr)
                },
                activationTime: json.activationTime * 1000
            };

            Object.assign(options, IVComponent.toOptions(json));
            console.log(`Loading ContentNode ${options.name}`);

            if (options.modelFile === null) {
                return Promise.resolve(ContentNodeFactory.createOfType(options, json));
            } else {
                return new Promise((resolve, reject) => {
                    let inLoading = [
                        Texture.fromJSON(json.texture, camera),
                        Util.AssetsLoader.promise(options.modelFile)];

                    Promise.all(inLoading).then(resolved => {
                        options.texture = resolved[0];
                        options.model = resolved[1];
                        resolve(ContentNodeFactory.createOfType(options, json));
                    }).catch(reason => reject(new Error(`Failed to load ContentNode '${json.name}'. Reason: ${reason}`)));
                });
            }
        }

        /**
         * Returns a promise of a specific subclass of ContentNode.
         *
         * @param {Object} options
         * @param {Object} json
         * @returns {Promise<ContentNode>}
         */
        static createOfType(options, json) {
            switch (json.type) {
                case 'static':
                    return Promise.resolve(new ContentNode(options));
                case 'audio':
                    return IVAudioNode.fromJSON(json, options);
                case 'textual':
                    return TextNode.fromJSON(json, options);
                case 'gateway':
                    options.target = json.target;
                    return Promise.resolve(new GatewayNode(options));
            }
        }
    }

    let skydomeTypeSymbols = {
        VIDEO: Symbol.for('video'),
        IMAGE: Symbol.for('image'),
        TEXTURE: Symbol.for('texture')
    };

    class Skydome extends IVComponent {
        /**
         * Skydome
         *
         * @param {{
         * name: String,
         * description: String,
         * type: Skydome.Type,
         * fileURL: String,
         * texture: Texture}} options
         *
         * @constructor
         * @extends {IVComponent}
         */
        constructor(options) {
            options = options || {};

            super(options);

            this.type = options.type || Skydome.Type.IMAGE;
            this.fileURL = options.fileURL || '';
            this.texture = options.texture || null;
            this.threejsMaterial = (this.texture) ? this.texture.threejsTexture : new Three.MeshBasicMaterial();
        }


        static get Type() {
            return skydomeTypeSymbols;
        }

        static fromJSON(json, camera) {
            console.log(`Loading skydome ${json.name}`);
            let dome = new Skydome(IVComponent.toOptions(json));
            let type = Symbol.for(json.type);

            if (type === Skydome.Type.IMAGE) {
                dome.setImage(Util.AssetsLoader.projectProtocol + json.file);
            } else if (type === Skydome.Type.VIDEO) {
                dome.setVideo(Util.AssetsLoader.projectProtocol + json.file);
            } else if (type === Skydome.Type.TEXTURE) {
                Texture.fromJSON(json.texture, camera).then(texture => {
                    dome.type = Skydome.Type.TEXTURE;
                    dome.texture = texture;
                    dome.threejsMaterial = dome.texture.threejsTexture;
                });
            }

            return dome;
        }

        toJSON() {
            let json = {
                id: this.id,
                name: this.name,
                description: this.description,
                type: Symbol.keyFor(this.type)
            };

            if (this.type === Skydome.Type.TEXTURE) {
                json.texture = this.texture;
            } else {
                json.file = Util.AssetsLoader.stripProtocol(this.fileURL);
            }

            return json;
        }

        getFiles() {
            let files = [];

            if (Util.AssetsLoader.internalAsset(this.fileURL)) {
                files.push(Util.AssetsLoader.getInternalAsset(this.fileURL));
            } else if (this.type === Skydome.Type.TEXTURE) {
                files.concat(this.texture.getFiles());
            }

            return files;
        }

        setImage(url) {
            this.type = Skydome.Type.IMAGE;
            this.fileURL = url;
        }

        setVideo(url) {
            this.type = Skydome.Type.VIDEO;
            this.fileURL = url;
            this.threejsMaterial = new Three.MeshBasicMaterial();
        }
    }

    class Scene extends IVComponent {
        /**
         * Scene
         *
         * @param {{
         * name: String,
         * description: String,
         * skydome: !Skydome}} options
         *
         * @constructor
         * @extends {IVComponent}
         */
        constructor(options) {
            options = options || {};
            super(options);

            this.skydome = options.skydome;
            this.nodes = new Map();
        }

        /**
         * @param {Object} json
         * @param {Three.Camera} camera
         * @returns {Promise<Scene>}
         */
        static fromJSON(json, camera) {
            return new Promise((resolve, reject) => {
                let options = IVComponent.toOptions(json);
                console.log(`Loading scene ${options.name}`);
                options.skydome = Skydome.fromJSON(json.skydome, camera);

                let scene = new Scene(options);

                Promise.all(json.nodes.map(node => ContentNodeFactory.fromJSON(node, camera))).then(resolved => {
                    resolved.forEach(node => scene.addContentNode(node));
                    resolve(scene);
                }).catch(reason => reject(new Error(`Failed to load scene '${scene.name}'. Reason: ${reason}`)));
            });
        }

        toJSON() {
            let nodes = [];
            for (let node of this.nodes) {
                nodes.push(node[1]);
            }

            return {
                id: this.id,
                name: this.name,
                description: this.description,
                skydome: this.skydome,
                nodes: nodes
            };
        }

        /**
         * See ContentNode.getFiles() for description
         *
         * @returns {Object[]}
         */
        getFiles() {
            let files = this.skydome.getFiles();

            let fileLists = [];
            for (let node of this.nodes) {
                fileLists.push(node[1].getFiles());
            }

            return files.concat(...fileLists);
        }

        /**
         * @param {ContentNode} node
         */
        addContentNode(node) {
            if (node instanceof ContentNode) {
                this.nodes.set(node.id, node);
            } else {
                console.error(node, 'is not of type IVProject.ContentNode, cannot add it to scene', this);
            }
        }

        /**
         * @param {ContentNode} node
         */
        removeContentNode(node) {
            this.nodes.delete(node.id);
        }

        /**
         * @param {ContentNode} node
         * @param {String} type
         */
        changeNodeToType(node, type) {
            if (node instanceof ContentNode) {
                let newNode = ContentNodeFactory.convert(type, node);
                if (newNode) {
                    this.nodes.set(node.id, newNode);
                } else {
                }
            } else {
            }
        }
    }

    class Project extends IVComponent {
        /**
         * Project
         *
         * @param {{
         * name: String,
         * description: String,
         * author: String,
         * location: String,
         * copyright: String}} options
         *
         * @constructor
         * @extends {IVComponent}
         */
        constructor(options) {
            options = options || {};

            options.name = options.name || 'New Project';
            options.description = options.description || 'This is a brand new project.';

            super(options);

            this.author = options.author || 'Jone Smith <jone.smith@smithonia.co.uk>';
            this.location = options.location || 'Earth';
            this.copyright = options.copyright || '© 2015 Jone Smith <jone.smith@smithonia.co.uk>';
            this.scenes = new Map();
            this._mainScene = options.mainScene || '';

            this.fmtvers = IVGlobals.ExportFileFormatVersion;
        }

        set mainScene(scene) {
            if (scene instanceof Scene) {
                this._mainScene = scene.id;
                this.scenes.set(scene.id, scene);
            } else {
                this._mainScene = scene;
            }
        }

        get mainScene() {
            return this.scenes.get(this._mainScene);
        }

        /**
         * @param {Object} json
         * @param {Three.Camera} camera
         * @returns {Promise<Project>}
         */
        static fromJSON(json, camera) {
            return new Promise((resolve, reject) => {
                let options = IVComponent.toOptions(json);
                console.log(`Loading project ${options.name}`);
                options.copyright = json.copyright;
                options.author = json.author;
                options.mainScene = json.mainScene;

                let project = new Project(options);

                let promisedScenes = json.scenes.map(scene => Scene.fromJSON(scene, camera));
                Promise.all(promisedScenes).then(resolved => {
                    resolved.forEach(scene => project.addScene(scene));
                    resolve(project);
                }).catch(reason => reject(new Error(`Failed to load project '${json.name}'. Reason: ${reason}`)));
            });
        }

        toJSON() {
            let scenes = [];
            for (let scene of this.scenes) {
                scenes.push(scene[1]);
            }

            return {
                name: this.name,
                description: this.description,
                location: this.location,
                copyright: this.copyright,
                author: this.author,
                fmtvers: this.fmtvers,
                mainScene: this._mainScene,
                scenes: scenes
            };
        }

        /**
         * Returns a list of files that have been generated in the editor
         * or are internal assets of the program.
         *
         * Each asset objects contains the following properties:
         *
         * - action: save or copy, depending on whether the assets is internal or generated
         * - to: String, the path and file name of the resulting file.
         * - from: Path of the internal asset, if action is copy.
         * - data: If the asset is generated one, contains the obj file data.
         *
         * @returns {Object[]}
         */
        getFiles() {
            let files = [];
            for (let scene of this.scenes) {
                files.push(...scene[1].getFiles());
            }

            // For JS noobs ... is called spread operator
            return files.sort((a, b) => {
                if (a.to < b.to) {
                    return -1;
                }
                if (a.to === b.to) {
                    return 0;
                }

                return 1;
            }).filter((item, pos, array) => (pos > 0 && item.to !== array[pos - 1].to));
        }

        /**
         * @param {Scene} scene
         */
        addScene(scene) {
            if (scene instanceof Scene) {
                this.scenes.set(scene.id, scene);
            } else {
                console.error(scene, 'is not of type IVProject.Scene, cannot add it to project', this);
            }
        }

        /**
         * @param {String} id
         * @returns {(Null | Scene)} scene ot null if not found
         */
        sceneByID(id) {
            let retval = this.scenes.get(id);
            if (typeof retval !== undefined) {
                return retval;
            } else {
                console.error('No scene by ID ', id, 'found from ', this);
                return null;
            }
        }
    }

    class DefaultsFactory {
        /**
         * @param {ContentNode} node
         * @param {Texture} texture
         * @param {String} modelUrl
         * @param {Three.Geometry} geometry
         * @returns {ContentNode}
         */
        static patchNode(node, texture, modelUrl, geometry) {
            node.texture = texture;
            node.modelFile = Util.AssetsLoader.assetsProtocol + modelUrl;
            node.setModel(geometry);
            return node;
        }

        static modelError(type, reason) {
            return new Error(`Failed to create default ${type}. Could not load 3D model. Reason: ${reason}`);
        }

        static textureError(type, reason) {
            return new Error(`Failed to create default ${type}. Could not load texture. Reason: ${reason}`);
        }

        /**
         * Creates a new node or patches and existring one with default speaker
         * model and textures.
         *
         * If an existing node is passed as the paramater it is patched.
         * To create a new node of type X then pass in the constructor as
         * the paramater.
         *
         * @param {(ContentNode | Function)} node – Existing ContentNode or constructor
         *
         * @returns {Promise<ContentNode>}
         */
        static promiseSpeaker(node) {
            let modelUrl = '/assets/models/speaker.obj';
            let map = '/assets/textures/speaker.png';
            let normal = '/assets/textures/speaker-specular.png';
            let specular = '/assets/textures/speaker-normal.png';

            return new Promise((resolve, reject) => {
                DefaultsFactory.promisePhong(map, normal, specular).then(texture => {
                    Util.AssetsLoader.promise(modelUrl).then(geometry => {
                        if (node instanceof ContentNode) {
                            resolve(DefaultsFactory.patchNode(node, texture, modelUrl, geometry));
                        } else {
                            resolve(new node({model: geometry, modelFile: modelUrl, texture: texture}));
                        }
                    }).catch(reason => reject(DefaultsFactory.modelError('speaker', reason)));
                }).catch(reason => reject(DefaultsFactory.textureError('speaker', reason)));
            });
        }

        /**
         * Creates a new node or patches and existring one with default arrow
         * model and textures.
         *
         * If an existing node is passed as the paramater it is patched.
         * To create a new node of type X then pass in the constructor as
         * the paramater.
         *
         * @param {(ContentNode | Function)} node – Existing ContentNode or constructor
         *
         * @returns {Promise<ContentNode>}
         */
        static promiseArrow(node, camera) {
            let modelUrl = '/assets/models/arrow.obj';

            return new Promise((resolve, reject) => {
                DefaultsFactory.promiseFresnel(camera).then(texture => {
                    Util.AssetsLoader.promise(modelUrl).then(geometry => {
                        if (node instanceof ContentNode) {
                            resolve(DefaultsFactory.patchNode(node, texture, modelUrl, geometry));
                        } else {
                            resolve(new node({model: geometry, modelFile: modelUrl, texture: texture}));
                        }
                    }).catch(reason => reject(DefaultsFactory.modelError('arrow', reason)));
                }).catch(reason => reject(DefaultsFactory.textureError('arrow', reason)));
            });
        }

        static promiseFresnel(camera) {
            let vertexUrl = '/assets/shaders/vertex/Fresnel-vertex.glsl';
            let fragmentUrl = '/assets/shaders/fragment/Fresnel-fragment.glsl';
            let uniforms = [
                {name: 'mRefractionRatio', type: 'f', value: 1.02},
                {name: 'mFresnelBias', type: 'f', value: 0.1},
                {name: 'mFresnelPower', type: 'f', value: 2.0},
                {name: 'mFresnelScale', type: 'f', value: 1.0},
                {name: 'tCube', type: 'camera', value: null},
                {name: 'mMinz', type: 'f', value: -1.0},
                {name: 'mLength', type: 'f', value: 2.0},
                {name: 'mDefaultColor', type: 'c', value: [1.0, 0.0, 0.0]},
                {name: 'mActivatedColor', type: 'c', value: [0.0, 1.0, 0.0]}
            ];

            return new Promise((resolve, reject) => {
                Texture.loadFromAssets(vertexUrl, fragmentUrl, uniforms, camera, Util.AssetsLoader.assetsProtocol)
                .then(texture => resolve(texture))
                .catch(reason => reject(reason));
            });
        }

        /**
         * @param {String=} texImage
         * @returns {Promise}
         */
        static promiseSimple(texImage) {
            let vertexUrl = '/assets/shaders/vertex/Simple-vertex.glsl';
            let fragmentUrl = '/assets/shaders/fragment/Simple-fragment.glsl';
            let uniforms = [{name: 'texture', type: 'img', value: texImage || '/assets/textures/CheckerBoard.png'}];

            return new Promise((resolve, reject) => {
                Texture.loadFromAssets(vertexUrl, fragmentUrl, uniforms, null, Util.AssetsLoader.assetsProtocol)
                .then(texture => resolve(texture))
                .catch(reason => reject(reason));
            });
        }

        /**
         * @param {Number[]=} dColor
         * @param {Number[]=} aColor
         * @returns {Promise}
         */
        static promiseColor(dColor, aColor) {
            let vertexUrl = '/assets/shaders/vertex/Simple-vertex.glsl';
            let fragmentUrl = '/assets/shaders/fragment/Color-fragment.glsl';
            let uniforms = [
                {name: 'dColor', type: 'c', value: dColor || [1, 0, 0]},
                {name: 'aColor', type: 'c', value: aColor || [0, 1, 0]}
            ];

            return new Promise((resolve, reject) => {
                Texture.loadFromAssets(vertexUrl, fragmentUrl, uniforms, null, Util.AssetsLoader.assetsProtocol)
                .then(texture => resolve(texture))
                .catch(reason => reject(reason));
            });
        }

        /**
         * @param {String=}  map
         * @param {String=} normalMap
         * @param {String=} specularMap
         * @returns {Promise}
         */
        static promisePhong(map, normalMap, specularMap) {
            let vertexUrl = '/assets/shaders/vertex/Phong-vertex.glsl';
            let fragmentUrl = '/assets/shaders/fragment/Phong-fragment.glsl';
            let uniforms = [
                {name: 'map', type: 'img', value: map || '/assets/textures/CheckerBoard.png'},
                {name: 'normalMap', type: 'img', value: normalMap || '/assets/textures/CheckerBoard.png'},
                {name: 'specularMap', type: 'img', value: specularMap || '/assets/textures/CheckerBoard.png'}
            ];

            return new Promise((resolve, reject) => {
                Texture.loadFromAssets(vertexUrl, fragmentUrl, uniforms, null, Util.AssetsLoader.assetsProtocol)
                .then(texture => resolve(texture))
                .catch(reason => reject(reason));
            });
        }

        /**
         * @returns {Promise<Skydome>}
         */
        static promiseDome() {
            return new Promise((resolve, reject) => {
                let skydomeUrl = '/assets/textures/CheckerBoard.png';
                let vertexUrl = '/assets/shaders/vertex/Simple-vertex.glsl';
                let fragmentUrl = '/assets/shaders/fragment/Simple-fragment.glsl';

                let uniforms = [{name: 'texture', type: 'img', value: skydomeUrl}];

                Texture.loadFromAssets(vertexUrl, fragmentUrl, uniforms, null, Util.AssetsLoader.assetsProtocol)
                    .then(texture => resolve(new Skydome({fileUrl: skydomeUrl, texture: texture})))
                    .catch(reason => reject(new Error(`Failed to create default skydome. Reason: ${reason}`)));
            });
        }
    }

    // Exports
    return {
        Project: Project,
        Scene: Scene,
        ContentNode: ContentNode,
        ContentNodeFactory: ContentNodeFactory,
        GatewayNode: GatewayNode,
        AudioNode: IVAudioNode,
        Skydome: Skydome,
        TextNode: TextNode,
        Texture: Texture,
        DefaultsFactory: DefaultsFactory
    };
});
