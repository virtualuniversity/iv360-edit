/**
 * Copyright 2015-2016 by Jesse Jaara <jesse.jaara@gmail.com>
 * Copyright 2015-2016 by Jussi Karhu <karhu.jussi.t@student.uta.fi>
 *
 * This file is part of IVEditor.
 *
 * IVEditor is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * IVEditor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with IVEditor.
 * If not, see <http://www.gnu.org/licenses/>.
 *
 * @license GPL-3.0+ <http://spdx.org/licenses/GPL-3.0+>
 */

define(['IVPlayer', 'Util'], function(IVPlayer, Util) {
    'use strict';

    var exports = {};

    var container = document.getElementById('container');
    var spinner = document.getElementById('spinner');
    var playbtn = document.getElementById('startPlayBtn');
    var dot = document.getElementById('centerSpot');
    var fsBtn = document.getElementById('fsBtn');
    var instructions = document.getElementById('instructions');
    var pbar = document.getElementById('pbar');
    var lstatus = document.getElementById('loadingStatus');

    function fullscreenEnabled() {
        return (document.fullscreenEnabled || document.webkitFullscreenEnabled ||
                document.mozFullScreenEnabled || document.msFullscreenEnabled);
    }

    function fullscreenElement() {
        if (document.fullscreenElement) {
            return document.fullscreenElement;
        } else if (document.mozFullScreenElement) {
            return document.mozFullScreenElement;
        } else if (document.webkitFullscreenElement) {
            return document.webkitFullscreenElement;
        } else if (document.msFullscreenElement) {
            return document.msFullscreenElement;
        }
    }

    function requestFullscreen() {
        if (document.body.requestFullscreen) {
            document.body.requestFullscreen();
        } else if (document.body.webkitRequestFullscreen) {
            document.body.webkitRequestFullscreen();
        } else if (document.body.mozRequestFullScreen) {
            document.body.mozRequestFullScreen();
        } else if (document.body.msRequestFullscreen) {
            document.body.msRequestFullscreen();
        }
    }

    function exitFullscreen() {
        if (document.exitFullscreen) {
            document.exitFullscreen();
        } else if (document.msExitFullscreen) {
            document.msExitFullscreen();
        } else if (document.mozCancelFullScreen) {
            document.mozCancelFullScreen();
        } else if (document.webkitExitFullscreen) {
            document.webkitExitFullscreen();
        }
    }

    function setupSettings() {
        var settingsBtn = document.getElementById('settingsBtn');
        var settingsContainer = document.getElementById('settingsContainer');
        var orientationToggle = document.getElementById('orientation');
        var stereoToggle = document.getElementById('stereo');

        settingsContainer.classList.remove('hidden');

        settingsBtn.addEventListener('click', function() {
            if (settingsContainer.classList.contains('opening')) {
                settingsContainer.classList.remove('opening');
                settingsContainer.classList.add('closing');
            } else {
                settingsContainer.classList.remove('closing');
                settingsContainer.classList.add('opening');
            }
        }, false);

        orientationToggle.addEventListener('change', e => {
            exports.controller.useOrientation = orientationToggle.checked;
        });

        stereoToggle.addEventListener('change', () => {
            fsBtn.classList.add('hidden');
            settingsContainer.classList.add('hidden');
            dot.classList.add('hidden');
            if (!fullscreenEnabled()) {
                toggleFullscreen();
            }
            exports.player.rendererOnUse = exports.player.stereo;
        });
    }

    function toggleFullscreen() {
        if (fullscreenEnabled()) {
            if (!fullscreenElement()) {
                requestFullscreen();
                fsBtn.classList.remove('fa-expand');
                fsBtn.classList.add('fa-compress');
            } else {
                exitFullscreen();
                fsBtn.classList.remove('fa-compress');
                fsBtn.classList.add('fa-expand');
            }
        }
    }

    function setupOrientationDetection() {
        var fn = function(e) {
            if (e.alpha) { // Chromium fires empty events on desktop
                setupSettings();
                exports.controller.setupOrientationControls();
                window.removeEventListener('deviceorientation', fn);
            }
        };

        window.addEventListener('deviceorientation', fn, false);
    }

    function playbtnClickHandler(player) {
        pbar.classList.add('hidden');
        lstatus.classList.add('hidden');
        dot.classList.remove('hidden');
        playbtn.classList.add('hidden');
        container.classList.remove('hidden');
        instructions.classList.add('hidden');
        fsBtn.classList.remove('hidden');
        fsBtn.addEventListener('click', toggleFullscreen, false);

        player.play();
    }

    function whenProjectLoaded(player) {
        playbtn.addEventListener('click', () => playbtnClickHandler(player));

        spinner.classList.add('hidden');
        playbtn.classList.remove('hidden');
        pbar.setAttribute('value', 100);
        lstatus.innerHTML = 'All loaded and ready to go!';
    }

    function whenPlayerReady(player) {
        exports.player = player;
        exports.controller = new IVPlayer.IVPlayerViewerController(player);
        setupOrientationDetection();

        var file = IVConfig.baseUrl + IVConfig.file;
        player.loadProject(file).then(() => whenProjectLoaded(player)).catch(reason => console.log(reason));
    }

    function init() {
        IVPlayer.IVPlayer.init(container).then(whenPlayerReady).catch(reason => console.log(reason));
    }

    function logProgress(e) {
        if (e.loaded === e.total) {
            lstatus.innerHTML = 'Processing files…';
            pbar.removeAttribute('value');
        } else {
            var loaded = (e.loaded / 1024 / 1024).toFixed(1);
            var total = (e.total / 1024 / 1024).toFixed(1);
            pbar.setAttribute('value', e.loaded / e.total * 100);
            lstatus.innerHTML = `Downloading assets bundle: ${loaded}MB/${total}MB`;
        }
    }

    var filesLoaded = 0;
    /**
     * Show the name of the file that was loaded from the zip bundle in the status area.
     *
     * @param {String} path
     */
    function showLoadingFile(path) {
        filesLoaded++;
        var parts = path.split('/');
        var file = parts[parts.length - 1];
        lstatus.innerHTML = `Files processed ${filesLoaded} <br> Last file ${file}`;
    }

    if (IVConfig.bundle) {
        var bundle = IVConfig.baseUrl + '/project/assets.zip';
        Util.AssetsLoader.populateCacheFromZip(bundle, logProgress, showLoadingFile).then(stats => {
            console.log('Files loaded from assets bundle:', stats);
            lstatus.innerHTML = 'Loading scenes…';

            init();
        }).catch(reason => console.log(reason));
    } else {
        lstatus.innerHTML = 'Loading files and preparing scenes…';
        init();
    }

    return exports;
});
