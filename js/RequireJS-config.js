var base = 'js';
if (typeof IVConfig === 'object') {
    base = IVConfig.baseUrl + '/js';
}

var require = {
    baseUrl: base,
    paths: {
        jquery: 'vendor/jquery',
        jqueryui: 'vendor/jquery-ui',
        three: 'vendor/three',
        dat: 'vendor/dat.gui',
        pako: 'vendor/pako',
        EpicEditor: 'vendor/epiceditor',
        FontAwesome: 'vendor/FontAwesome',
        Hammer: 'vendor/hammer',
        JSZip: 'vendor/JSZip',
        MarkdownIt: 'vendor/markdown-it',
        OBJParser: 'vendor/OBJParser',
        OBJExporter: 'vendor/OBJExporter',
        PrettyJSON: 'vendor/PrettyJSON',
        ReplaceLink: 'vendor/ReplaceLink',
        StereoEffect: 'vendor/StereoEffect'
    },
    shim: {
        JSZip: {
            deps: ['pako']
        }
    }
};

if (typeof exports === 'object') {
    exports.config = require;
}
