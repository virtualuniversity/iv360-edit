/**
 * Copyright 2015-2016 by Jesse Jaara <jesse.jaara@gmail.com>
 * Copyright 2015-2016 by Jussi Karhu <karhu.jussi.t@student.uta.fi>
 *
 * This file is part of IVEditor.
 *
 * IVEditor is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * IVEditor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with IVEditor.
 * If not, see <http://www.gnu.org/licenses/>.
 *
 * @license GPL-3.0+ <http://spdx.org/licenses/GPL-3.0+>
 */

define(['three', 'IVGlobals', 'MarkdownIt', 'ReplaceLink', 'pako', 'JSZip', 'OBJParser'],
function(Three, IVGlobals, MarkdownIt, ReplaceLink, Pako, JSZip, OBJParser) {
    'use strict';

    class SPCoordinate {
        /**
         * SPCoordinate represents a spherical coordinate and contains conversion
         * functions to and from cartesian coordinate system.
         *
         * Individual coordinate components can be accessed and assigned thought
         * following properties:
         * @prop {Number} radius - Distance from the center of the sphere
         * @prop {Number} zenith - Elevation from the xz-plane in degrees
         * @prop {Number} zenithR - Radian equivalent of zenith.
         * @prop {Number} azimuth - Rotational distance to right, around y-axis, in degrees.
         *                          Zero is located on the positive side of the z-axis.
         * @prop {Number} azimuthR - Radian equivalent of azimuth.
         *
         * Assigning a value to zenith/zenithR or azimuth/azimuthR will also update
         * their radian or degree counterparts.
         *
         * @param {Number} radius - [0, ∞], will be clamped to lower range
         * @param {Number} zenith - [-90°, 90°], will be clamped to the range
         * @param {Number} azimuth - [0°, 360°], will be converted to range
         * @param {Boolean=} inRadians - values are in radians
         * @constructor
         */
        constructor(radius, zenith, azimuth, inRadians) {
            radius = radius || 0;
            zenith = zenith || 0;
            azimuth = azimuth || 0;

            this.set(radius, zenith, azimuth, inRadians);
        }

        set radius(radius) { this._radius = (radius < 0) ? 0 : radius; }
        get radius() { return this._radius; }

        set zenith(zenith) {
            this._zenith = Three.Math.clamp(zenith, -90, 90);
            this._zenithR = this._zenith * IVGlobals.PI_PER_180;
        }
        get zenith() { return this._zenith; }


        set zenithR(zenith) {
            this._zenithR = Three.Math.clamp(zenith, -Math.PI, Math.PI);
            this._zenith = this._zenithR * IVGlobals.RAD_IN_DEG ;
        }
        get zenithR() { return this._zenithR; }

        set azimuth(azimuth) {
            this._azimuth = azimuth % 360;
            if (azimuth < 0) {
                this._azimuth += 360;
            }

            this._azimuthR = this._azimuth * IVGlobals.PI_PER_180;
        }
        get azimuth() { return this._azimuth; }

        set azimuthR(azimuth) {
            this._azimuthR = azimuth % IVGlobals.TWO_PI;
            if (this._azimuthR < 0) {
                this._azimuthR += IVGlobals.TWO_PI;
            }

            this._azimuth = this._azimuthR * IVGlobals.RAD_IN_DEG ;
        }
        get azimuthR() { return this._azimuthR; }

        /**
         * Convenience method to update all values of the coordinate point at once.
         *
         * @param {Number} radius - [0, ∞], will be clamped to lower range
         * @param {Number} zenith - [-90, 90], will be clamped to the range
         * @param {Number} azimuth - [0, 360], will be converted to range
         * @param {Boolean} inRadians - values are in radians
         */
        set(radius, zenith, azimuth, inRadians) {
            this.radius = radius;

            if (inRadians) {
                this.zenithR = zenith;
                this.azimuthR = azimuth;
            } else {
                this.zenith = zenith;
                this.azimuth = azimuth;
            }
        }

        /**
         * Get a cartesian coordinate equivalent of this point.
         *
         * @returns {THREE.Vector3}
         */
        toCartesian() {
            return SPCoordinate.toCartesian(this.radius, this.zenithR, this.azimuthR);
        }

        /**
         * Get a Euler rotatian in YXZ order representing this point.
         *
         * @returns {Three.Euler}
         */
        toEuler() {
            return new Three.Euler(this._zenithR, this._azimuthR, 0, 'YXZ');
        }

        /**
         * Checks whether this point is between ul and lr.
         *
         * @param {SPCoordinate} ul
         * @param {SPCoordinate} lr
         * @returns {Boolean}
         */
        between(ul, lr) {
            let distance = Math.abs(ul.azimuth - lr.azimuth);

            if (this.zenith > lr.zenith && this.zenith < ul.zenith) {
                if (distance > 180) {
                    console.log('Crossing 0 boundary');
                    if (this.azimuth > lr.azimuth || this.azimuth < ul.azimuth) {
                        return true;
                    }
                } else {
                    if (this.azimuth > lr.azimuth && this.azimuth < ul.azimuth) {
                        return true;
                    }
                }
            }

            return false;
        }

        /**
         * Tells in which direction one needs to travel along azimuth in order
         * to reach the azimuth angle b in least time.
         * Min(forwards distance, backwards distance). If the distance is exactly
         * 180° forward distance is assumed.
         *
         * Return values:
         * - 1: forward
         * - 0: azimuthal angles are equal
         * - -1: backwards
         *
         * @param {SPCoordinate | Number} b
         * @returns {Number} - direction
         */
        azimuthDirection(b) {
            let bAzimuth = b.azimuth || b;

            if (bAzimuth === this.azimuth) {
                return 0;
            }

            return (bAzimuth - this.azimuth < 180) ? 1 : -1;
        }

        /**
         * Normalized version of toCartesian.
         *
         * returns {Three.Vector3}
         */
        frontVector() {
            return SPCoordinate.toCartesian(1, this.zenithR, this.azimuthR);
        }

        /**
         * Offset 90° from frontVector on zenith..
         *
         * returns {Three.Vector3}
         */
        upVector() {
            let zenith = this.zenith + 90;
            let azimuth = this.azimuth;
            if (zenith > 90) {
                zenith = 90 - this.zenith;
                azimuth += 180;
            }

            return SPCoordinate.toCartesian(1, zenith * IVGlobals.PI_PER_180, azimuth * IVGlobals.PI_PER_180);
        }

        /**
         * Serializes this coordinate into array.
         *
         * arr[0]: radius
         * arr[1]: zenith in degrees
         * arr[2]: azimuth in degrees
         *
         * @returns {Array} – [radius, zenith, azimuth]
         */
        toJSON() {
            return [this.radius / IVGlobals.SCALE, this.zenith, this.azimuth];
        }

        /**
         * Real implementation for the member function of the same name.
         * Gets an cartesian coordinate system version of the given point.
         *
         * @param {Number} radius
         * @param {Number} zenithR
         * @param {Number} azimuthR
         * @returns {THREE.Vector3}
         */
        static toCartesian(radius, zenithR, azimuthR) {
            let cartesian = new Three.Vector3();

            cartesian.x = radius * Math.cos(zenithR) * Math.cos(azimuthR);
            cartesian.y = radius * Math.sin(zenithR);
            cartesian.z = radius * Math.cos(zenithR) * Math.sin(azimuthR);

            return cartesian;
        }

        /**
         * Create a new SPCoordinate from a cartesian coordinate system point.
         *
         * @param {THREE.Vector3} point
         * @returns {SPCoordinate}
         */
        static fromCartesian(point) {
            let r = point.length();
            let zenith = Math.atan2(point.y, Math.sqrt(Math.pow(point.x, 2) + Math.pow(point.z, 2)));
            let azimuth = Math.atan2(point.z, point.x);

            return new SPCoordinate(r, zenith, azimuth, true);
        }

        /**
         * Create a new SPCoordinate from an array.
         *
         * arr[0]: radius
         * arr[1]: zenith in degrees
         * arr[2]: azimuth in degrees
         *
         * @param {Array<Number>} arr
         */
         static fromArray(arr) {
            return new SPCoordinate(arr[0], arr[1], arr[2]);
        }
    }

    class SPBox {
        /**
         * Represents a rectangle centered around an SPCoordinate point.
         *
         * Exposes properties named up, down, left and right, which describe the distance
         * in the given direction from the point given.
         *
         * All parameters default to 1.
         *
         * @param {Number=} up
         * @param {Number=} down
         * @param {Number=} left
         * @param {Number=} right
         * @constructor
         */
        constructor(up, down, left, right) {
            this.up = up || 1;
            this.down = down || 1;
            this.left = left || 1;
            this.right = right || 1;
        }

        /**
         * Get 2 corner points upper left and lower right, that describe the rectangle, as SPCoordinates.
         *
         * @param {SPCoordinate} point
         * @returns {{
         * ul: SPCoordinate,
         * lr: SPCoordinate}}
         */
        cornersForPoint(point) {
            return {
                ul: new SPCoordinate(point.radius, point.zenith + this.up, point.azimuth - this.left),
                lr: new SPCoordinate(point.radius, point.zenith - this.down, point.azimuth + this.right)
            };
        }

        /**
         * Construct a new SPBox relative to given center point from the given corner points.
         *
         * @param {SPCoordinate} center — top, down, left and right will be realiteve to this
         * @param {SPCoordinate} ul — Upper left corner
         * @param {SPCoordinate} lr — Lower right corner
         * @returns {SPBox}
         */
        static fromCorners(center, ul, lr) {
            let left = Math.abs(ul.azimuth - center.azimuth);
            if (left > 180) {
                left = 360 - left;
            }

            let right = Math.abs(ul.azimuth - center.azimuth);
            if (right > 180) {
                right = 360 - right;
            }

            return new SPBox(
                Math.abs(ul.zenith - center.zenith),
                Math.abs(lr.zenith - center.zenith),
                left,
                right
            );
        }
    }

    class TriggeringCounter {
        /**
         * Counter that triggers a callback function when the internal counter reaches
         * the designated value.
         *
         * @param {Number} countTo
         * @param {Function} callback — Fire when counter hits countTo-value
         * @constructor
         */
        constructor(countTo, callback) {
            this.to = countTo;
            this.value = 0;
            this.callback = callback;
            this.fired = false;
        }

        /**
         * Set the internal counter to value.
         *
         * @param {Number} value
         */
        set(value) {
            this.tick(value - this.value);
        }

        /**
         * Count up by one.
         */
        up() { this.tick(1); }

        /**
         * Count down by one.
         */
        down() { this.tick(-1); }

        /**
         * Add the given delta to the internal counter.
         * Clamps to [0, countTo].
         *
         * @param {Number} delta — tick up or down the ammount specified by the delta value.
         */
        tick(delta) {
            let old = this.value;
            this.value = Three.Math.clamp(this.value + delta, 0, this.to);

            this.fired = (this.value === old);

            if (this.value === this.to && !this.fired) {
                this.callback();
                this.fired = true;
            }
        }
    }

    class ZipStats {
        /**
         * A helper object for file count statistic in a zip file.
         *
         * @constructor
         */
        constructor() {
            this.audios = 0;
            this.images = 0;
            this.geometries = 0;
            this.shaders = 0;
        }
    }

    class FileCache {
        /**
         * A file cache for AssetsLoader.
         *
         * @constructor
         */
        constructor() {
            this.files = {};
        }

        /**
         * Check whether the given file has a record in the cache.
         *
         * @param {String} url — File to check
         * @returns {Boolean}
         */
        has(url) { return this.files[url]; }

        /**
         * Initialize a new cache entry for the given URL.
         *
         * @param {String} url — File for which to initialize a new entry.
         */
        init(url) {
            this.files[url] = {
                data: undefined,
                waiting: []
            };
        }

        /**
         * Mark the given file as downloaded and add the actual data into the cache.
         * Fulfils any requests made for the file between init and finish calls.
         *
         * @param {String} url — File to mark as ready
         * @param {*} data — File contents
         */
        finish(url, data) {
            let item = this.files[url];
            item.data = data;

            for (let resolver of item.waiting) {
                resolver(data);
            }
        }

        /**
         * Request a file from the cache if the file has not been downloaded yet,
         * the returned promise waits till it is finished.
         *
         * @param {String} url — File to request
         * @returns {Promise<*>}
         */
        request(url) {
            return new Promise(resolve => {
                let item = this.files[url];

                if (item.data === undefined) {
                    item.waiting.push(resolve);
                } else {
                    resolve(item.data);
                }
            });
        }
    }

    class AutomatedAudioNode {
        /**
         * Easy to use "PannerNode". Automates creating and connecting a
         * new AudioBufferSourceNode, every time it is stopped or played till end.
         *
         * Exposes following read-write properties:
         *
         * - gain: Playback volume.
         * - position: SPCoordinate describing the location of the audio source.
         * - loop: Boolean describing whether the audio should loop.
         *
         * @param {AudioContext} audioContext
         * @param {BufferSource} buffer
         * @param {SPCoordinate} position
         * @constructor
         */
        constructor(audioContext, buffer, position) {
            this._loop = false;
            this.playing = false;

            this.audioCtx = audioContext;
            this.buffer = buffer;

            this.panner = this.audioCtx.createPanner();
            this.panner.connect(this.audioCtx.destination);

            this._gain = this.audioCtx.createGain();
            this._gain.connect(this.panner);

            this.node = null;
            this.setupNode();

            this.position = position || new SPCoordinate(10, 0, 0);
        }


        /** @param {Number} value - [0, 1] */
        set gain(value) { this._gain.gain.value = value; }

        /** @returns {Number} value - [0, 1] */
        get gain() { return this._gain.gain.value; }

        /** @param {SPCoordinate} position */
        set position(position) {
            this._position = position;
            let cartesian = position.toCartesian();
            this.panner.setPosition(cartesian.x, cartesian.y, cartesian.z);
        }

        /** @returns {SPCoordinate} */
        get position() { return this._position; }

        /** @param {Boolean} value */
        set loop(value) {
            this._loop = value;

            if (this.playing) {
                this.stop();
                this.start();
            }
        }

        /** @returns {Boolean}*/
        get loop() { return this._loop; }

        /**
         * Start the audio playback.
         */
        play() {
            if (!this.playing) {
                this.node.start();
                this.playing = true;
            }
        }

        /**
         * Stop the audio playback.
         */
        stop() {
            if (this.playing) {
                this.node.stop();
                this.node.disconnect();
                this.setupNode();
                this.playing = false;
            }
        }

        /**
         * Setup the inter WebAudio API nodes.
         *
         * Helper function for the constructor.
         */
        setupNode() {
            this.node = this.audioCtx.createBufferSource();
            this.node.buffer = this.buffer;
            this.node.loop = this._loop;
            this.node.onended = () => this.stop();
            this.node.connect(this._gain);
        }
    }

    // region: File extension regular expressions

    const AudioRegExp  = new RegExp('\.(mp3|m4a|acc|wav|ogg)$', 'i');
    const ImageRegExp  = new RegExp('\.(jpg|jpeg|png|ktx)$', 'i');
    const OBJRegExp    = new RegExp('\.obj$', 'i');
    const ShaderRegExp = new RegExp('\.glsl$', 'i');

    // endregion: File extension regular expressions

    // region: AssetsLoader

    class AssetsLoader {
        /**
         * Unified file loader for all file types from both application internal and project
         * scope assets.
         *
         * @constructor
         */
        constructor() {
            if (typeof IVConfig !== 'undefined') {
                this.assetsRoot = IVConfig.baseUrl + IVConfig.assetsPath;
                this.projectRoot = IVConfig.baseUrl + IVConfig.projectPath;
            } else {
                this.assetsRoot = '';
                this.projectRoot = '';
            }

            this.cache = new FileCache();
        }


        /**
         * Decodes and resamples ArrayBuffer into AudioBuffer.
         *
         * @param {ArrayBuffer} data
         * @returns {Promise<AudioBuffer>}
         */
        static processAudio(data) {
            return new Promise((resolve, reject) => {
                IVGlobals.AudioContext.decodeAudioData(data, resolve, reject);
            });
        }

        /**
         * Loads data into img element and wraps it in Three.js Texture.
         *
         * @param {ArrayBuffer} data
         * @returns {Promise<Three.Texture>}
         */
        static processImage(data) {
            return new Promise((resolve, reject) => {
                let img = document.createElement('img');
                img.addEventListener('error', reject);
                img.addEventListener('load', () => {
                    let tex = new Three.Texture(img);
                    tex.needsUpdate = true;
                    resolve(tex);
                });

                img.src = URL.createObjectURL(new Blob([data]));
            });
        }

        /**
         * Strip the 'assets:' or 'project:' protocol specifier from the given URL.
         *
         * @param {String} url — AssetsLoader URL with a scope protocol specifier
         * @return {String}
         */
        stripProtocol(url) {
            return url.replace(this.assetsProtocol, '').replace(this.projectProtocol, '');
        }

        /**
         * Initializes a XMLHttpRequest download.
         * Returns a promises that resolves to the response data.
         *
         * @param {String }url — AseetsLoader formatted URL
         * @param {function(ProgressEvent)=} onProgress — Bound to XMLHttpRequest progress event
         * @param {String=} responseType — XMLHttpRequest responseType property, defaults to 'text'
         * @returns {Promise<*>}
         */
        download(url, onProgress, responseType) {
            return new Promise((resolve, reject) => {
                let request = new XMLHttpRequest();

                request.responseType = responseType || 'text';
                request.addEventListener('load', () => resolve(request.response), false);
                request.addEventListener('error', reject, false);

                if (onProgress) {
                    request.addEventListener('progress', onProgress, false);
                }

                request.open('GET', url);
                request.send();
            });
        }

        /**
         * Primary download function. Returns a promise that resolves into the
         * downloaded and processed data file.
         *
         * - Image files will resolve into Three.js Texture objects.
         * - Audio files will resolve into AudiBuffer objects.
         * - Wavefront obj files will resolve into Three.js BufferedGeometry objects.
         * - Shader files will resolve into Strings.
         *
         * All other file types will result in reject due to unkown file type.
         *
         * @param {String} url — AssetsLoader URL with scope protocol specifier
         * @param {function(ProgressEvent)=} onProgress — Bound to XMLHttpRequest progress event
         * @return {Promise<*>}
         */
        promise(url, onProgress) {
            return new Promise((resolve, reject) => {
                let realUrl = this.getRealUrl(url);

                if (this.cache.has(realUrl)) {
                    this.cache.request(realUrl).then(data => resolve(data));
                } else {
                    this.cache.init(realUrl);

                    let cache = this.cache;
                    let cacheAndResolve = function (data) {
                        cache.finish(realUrl, data);
                        resolve(data);
                    };

                    if (url.match(ImageRegExp)) {
                        this.download(realUrl, onProgress, 'arraybuffer').then(data => {
                            AssetsLoader.processImage(data).then(cacheAndResolve);
                        });
                    } else if (url.match(OBJRegExp)) {
                        this.download(realUrl, onProgress).then(data => cacheAndResolve(OBJParser(data)));
                    } else if (url.match(ShaderRegExp)) {
                        this.download(realUrl, onProgress).then(cacheAndResolve);
                    } else if (url.match(AudioRegExp)) {
                        this.download(realUrl, onProgress, 'arraybuffer').then(data => {
                            AssetsLoader.processAudio(data).then(cacheAndResolve);
                        });
                    } else {
                        reject(new Error(`Unkown file extension. Cannot load file ${url}`));
                    }
                }
            });
        }

        /**
         * Returns a real URL for the given AssetsLoader URL.
         *
         * @param {String} url — AssetsLoader URL with scope protocol specifier
         * @returns {String}
         */
        getRealUrl(url) {
            return url.replace(this.assetsProtocol, this.assetsRoot)
            .replace(this.projectProtocol, this.projectRoot);
        }

        /**
         * Tells whether the given AssetsLoader URL refers to an application scope asset.
         *
         * @param {String} file
         * @returns {Boolean}
         */
        internalAsset(file) { return (file.includes(this.assetsProtocol)); }

        /**
         * Generates an action object for the server backend instruction it to
         * copy an internal asset into current project.
         *
         * @param {String} file
         * @returns {{action: String, to: String, from: String}}
         */
        getInternalAsset(file) {
            let clean = this.stripProtocol(file);
            return {
                action: 'copy',
                to: clean,
                from: clean
            };
        }

        /**
         * Add the given file to the cache.
         * Should only be called internally.
         *
         * @param {String} path
         * @param {Object} data
         * @param {function(String)} onFileLoad – Filename as parameter
         */
        populateFile(path, data, onFileLoad) {
            let url = this.getRealUrl(this.projectProtocol + '/' + path);
            this.cache.files[url] = {
                data: data,
                waiting: []
            };

            onFileLoad(path);
        }

        /**
         * Load and populate the cache with audio buffers from the zip file.
         *
         * @param {JSZip} zf
         * @param {ZipStats} stats
         * @param {function(String)} onFileLoad – Called once per each loaded file. Filename as parameter
         */
        populateAudios(zf, stats, onFileLoad) {
            let promised = [];

            zf.file(AudioRegExp).forEach(file => {
                promised.push(new Promise((resolve, reject) => {
                    AssetsLoader.processAudio(file.asArrayBuffer()).then(decoded => {
                        this.populateFile(file.name, decoded, onFileLoad);
                        stats.audios++;
                        resolve(true);
                    }).catch(reason => reject({file: file.name, reason: reason}));
                }));
            });

            return Promise.all(promised);
        }

        /**
         * Load and populate the cache with images from the zip file.
         *
         * @param {JSZip} zf
         * @param {ZipStats} stats
         * @param {function(String)} onFileLoad – Called once per each loaded file. Filename as parameter
         */
        populateImages(zf, stats, onFileLoad) {
            let promised = [];

            zf.file(ImageRegExp).forEach(file => {
                promised.push(new Promise((resolve, reject) => {
                    AssetsLoader.processImage(file.asArrayBuffer()).then(tex => {
                        this.populateFile(file.name, tex, onFileLoad);
                        stats.images++;
                        resolve(true);
                    }).catch(reason => reject({file: file.name, reason: reason}));
                }));
            });

            return Promise.all(promised);
        }

        /**
         * Load and populate the cache with geometries from the zip file.
         *
         * @param {JSZip} zf
         * @param {ZipStats} stats
         * @param {function(String)} onFileLoad – Called once per each loaded file. Filename as parameter
         */
        populateGeometries(zf, stats, onFileLoad) {
            let promised = [];

            zf.file(OBJRegExp).forEach(file => {
                promised.push(new Promise(resolve => {
                    this.populateFile(file.name, OBJParser(file.asText()), onFileLoad);
                    stats.geometries++;

                    resolve(true);
                }));
            });

            return Promise.all(promised);
        }

        /**
         * Load and populate the cache with shaders from the zip file.
         *
         * @param {JSZip} zf
         * @param {ZipStats} stats
         * @param {function(String)} onFileLoad – Called once per each loaded file. Filename as parameter
         */
        populateShaders(zf, stats, onFileLoad) {
            let promised = [];

            zf.file(ShaderRegExp).forEach(file => {
                promised.push(new Promise(resolve => {
                    this.populateFile(file.name, file.asText(), onFileLoad);
                    stats.shaders++;
                    resolve(true);
                }));
            });

            return Promise.all(promised);
        }

        /**
         * Load a zip file and populate the Cache with it's contents.
         * The cache should only be populated before any init() calls.
         * Populating the cache afterwarsds might result in corrupted
         * cache state.
         *
         * @param {String} url
         * @param {function(ProgressEvent)} onProgress
         * @param {function(String)} onFileLoad – Called once per each loaded file. Filename as parameter
         * @returns {Promise<ZipStats>} - Number of loaded items per type
         */
        populateCacheFromZip(url, onProgress, onFileLoad) {
            return new Promise((resolve, reject) => {
                let request = new XMLHttpRequest();
                request.responseType = 'arraybuffer';
                request.addEventListener('progress', onProgress);
                request.addEventListener('error', () => reject('Failed to load resource bundle.'));
                request.addEventListener('load', e => {
                    this.parseZipBuffer(e.target.response, onFileLoad)
                    .then(status => resolve(status))
                    .catch(reason => reject(reason));
                });

                request.open('GET', url, true);
                request.send();
            });
        }

        /**
         * Parse ArrayBuffer as zip file and populate the Cache with it's contents.
         *
         * @param {ArrayBuffer} buffer
         * @param {function(String)} onFileLoad – Called once per each loaded file. Filename as parameter
         * @returns {Promise<ZipStats>} - Number of loaded items per type
         */
        parseZipBuffer(buffer, onFileLoad) {
            return new Promise((resolve, reject) => {
                try {
                    let zf = new JSZip(buffer);
                    let stats = new ZipStats();

                    let promised = [
                        this.populateAudios(zf, stats, onFileLoad),
                        this.populateImages(zf, stats, onFileLoad),
                        this.populateGeometries(zf, stats, onFileLoad),
                        this.populateShaders(zf, stats, onFileLoad)
                    ];

                    Promise.all(promised)
                    .then(() => resolve(stats))
                    .catch(r => reject(`Could not load file ${r.file} from assets bundle. Reason: ${r.reason}`));
                } catch (error) {
                    reject(`Could not load zip file. Reason: ${error}`);
                }
            });
        }

        get assetsProtocol() { return 'assets:'; }
        get projectProtocol() { return 'project:'; }
    }

    let AssetsLoaderSingleton = new AssetsLoader();

    // endregion: AssetsLoader

    class Uniform {
        /**
         * IVEditor representation of an shader uniform as desribed in the datasheet.
         * @link {https://bitbucket.org/virtualuniversity/documents/downloads/Virtual University: Video Project Dataformat Specification.pdf}
         *
         * @param {String} type
         * @param {Number | Number[] | String} value
         * @constructor
         */
        constructor(type, value) {
            this.type = type;
            this.value = value;
        }

        /**
         * Returns a promise that resolves into a Three.js representation of this uniform.
         *
         * @param {Three.CubeCamera} camera — The camera to use if this a cube camera unform.
         * @returns {Promise<{type: String, value: *}>}
         */
        toThreeJS(camera) {
            return new Promise((validate, reject) => {
                let uniform = {type: this.type, value: this.value};

                if (this.type === 'img') {
                    Uniform.loadImage(uniform, this.value, validate, reject);
                } else {
                    if (['c', 'v2', 'v3', 'v4'].indexOf(this.type) !== -1) {
                        uniform.value = JSONToVector(this);
                    } else if (['m3', 'm4'].indexOf(this.type) !== -1) {
                        uniform.value = JSONToMatrix(this);
                    } else if (this.type === 'camera') {
                        uniform.type = 't';
                        uniform.value = camera;
                    }

                    validate(uniform);
                }
            });
        }

        /**
         * Loads the given image file pointed by an AssetsLoader URL into the given uniform.
         *
         * Helper function for the toThreeJS funtion.
         *
         * @param {Uniform} uniform — uniform to decorate
         * @param {String} url — AssetsLoader URL
         * @param {function(Uniform)} validate — Promise reolve function
         * @param {function(*)} reject — Promise reject funtion
         */
        static loadImage(uniform, url, validate, reject) {
            uniform.type = 't';
            AssetsLoaderSingleton.promise(url).then(texture => {
                uniform.value = texture;
                validate(uniform);
            }).catch(reason => reject(reason));
        }
    }

    class Uniforms {
        /**
         * Proxy object for maintaining a collection of IVEditor uniforms together
         * with their Three.JS counterparts.
         *
         * @param {Object} uniforms — Dictionary of Uniform objects
         * @constructor
         */
        constructor(uniforms) {
            this.uniforms = uniforms || {};
            this.updated = true;
            this.threejsUnimap = {};
        }

        /**
         * Constructs a Uniforms object from a serialized JSON object.
         *
         * @param {Object[]} json
         * @returns {Uniforms}
         */
        static fromJSON(json) {
            return new Uniforms(Uniforms.parseJSON(json, AssetsLoaderSingleton.projectProtocol));
        }

        /**
         * Converts an IVEditor JSON-uniforms array of {name, type, value} objects to
         * a dictionary based object format.
         *
         * @param {Object[]} json
         * @param {String} filePrefix
         * @returns {Object}
         */
        static parseJSON(json, filePrefix) {
            filePrefix = filePrefix || '';
            let unis = {};
            for (let uni of json) {
                let value = uni.value;

                if (uni.type === 'img') {
                    value = filePrefix + value;
                }

                unis[uni.name] = new Uniform(uni.type, value);
            }

            return unis;
        }

        /**
         * Serializes the uniforms into IVEditor JSON format.
         * That is an array of {name, type, value} objects.
         *
         * @returns {Array}
         */
        toJSON() {
            return this.map((key, val) => {
                let json = {name: key, type: val.type, value: val.value};
                if (json.type === 'img') {
                    json.value = AssetsLoaderSingleton.stripProtocol(json.value);
                }
                return json;
            });
        }

        /**
         * Get a list of backend server action object for all internal assets, needed
         * in the uniforms.
         *
         * @returns {Array}
         */
        getFiles() {
            let files = [];

            for (let uniform of this) {
                if (uniform[1].type === 'img' && AssetsLoaderSingleton.internalAsset(uniform[1].value)) {
                    files.push(AssetsLoaderSingleton.getInternalAsset(uniform[1].value));
                }
            }

            return files;
        }

        /**
         * Convert all uniforms into Three.js format.
         *
         * @param {Three.CubeCamera} camera — Camera to be used as the cube camera on camera uniforms.
         * @returns {Promise<Object>}
         */
        toThreeJS(camera) {
            return new Promise((resolve, reject) => {
                if (this.updated === true) {
                    this.updateThreejsUnimap(camera, resolve, reject);
                } else {
                    resolve(this.threejsUnimap);
                }
            });
        }

        /**
         * Actual conversion logick for toThreeJS.
         *
         * @param {Three.CubeCamera} camera — Camera to be used as the cube camera on camera uniforms.
         * @param {Function} resolve — Promise's resolve handle to call when all done.
         * @param {Function} reject — Promise's reject handle to call on error.
         */
        updateThreejsUnimap(camera, resolve, reject) {
            let threejsUnimap = {};

            let keys = Object.keys(this.uniforms);
            let processed = 0;
            keys.forEach(uni => {
                this.uniforms[uni].toThreeJS(camera).then(threejsuni => {
                    threejsUnimap[uni] = threejsuni;
                    processed++;

                    if (processed === keys.length) {
                        this.threejsUnimap = threejsUnimap;
                        this.updated = false;
                        resolve(this.threejsUnimap);
                    }
                }).catch(reject);
            });
        }

        /**
         * Fetches the uniform associated with the key.
         *
         * @param {String} key — Name of the uniform to fetch.
         * @returns {{
         * type: String,
         * value: *}}
         */
        get(key) {
            return this.uniforms[key];
        }

        /**
         * Adds or updates the given key-value pair.
         *
         * @param {String} key — Name of the uniform to set.
         * @param {{
         * type: String,
         * value: *}} value — Value of the uniform to set.
         */
        set(key, value) {
            this.uniforms[key] = value;
            this.updated = true;
        }

        /**
         * Removes the unform associated with the given key from the list.
         *
         * @param {String} key — Name of the uniform to remove.
         */
        unset(key) {
            delete this.uniforms[key];
            this.updated = true;
        }

        /**
         * Renames the given uniform.
         *
         * @param {String} oldKey
         * @param {String} newKey
         */
        rename(oldKey, newKey) {
            this.set(newKey, this.get(oldKey));
            this.unset(oldKey);
        }

        /**
         * Returns an iterator over the [key, value] pairs of the uniforms.
         *
         * @returns {{next: next, _map: *, _keys: Array, _position: Number}}
         */
        [Symbol.iterator]() {
            return {
                next: function () {
                    if (this._position < this._keys.length) {
                        let key = this._keys[this._position];
                        let uni = this._map[key];

                        this._position++;

                        return {done: false, value: [key, uni]};
                    } else {
                        return {done: true, value: undefined};
                    }
                },

                _map: this.uniforms,
                _keys: Object.keys(this.uniforms),
                _position: 0
            };
        }

        /**
         * Map the the uniforms to an array.
         *
         * @param {function(String, Uniform): Object} cb
         * @returns {Array}
         */
        map(cb) {
            let mapped = [];
            for (let uni of this) {
                mapped.push(cb(uni[0], uni[1]));
            }

            return mapped;
        }
    }

    // region: TextFrame

    /**
     * Callback for MarkdownIt's ReplaceLink plugin.
     * Convers all relative image links either AssetsLoader's FileCache entries
     * or real URL's if they have not been cached.
     *
     * Should not be used outside of MarkdownIt's constructor.
     *
     * @param link
     * @param env
     * @param token
     * @returns {*}
     */
    function prefixImageLink(link, env, token) {
        if (token.type === 'image') {
            let url = AssetsLoaderSingleton.projectRoot + link;
            if (AssetsLoaderSingleton.cache.has(url)) {
                return AssetsLoaderSingleton.cache.files[url].data.image.src;
            }

            return url;
        }

        return link;
    }

    let markDownRendererSingleton = (new MarkdownIt()).use(ReplaceLink, prefixImageLink);

    class TextFrame {
        /**
         * Represents a single frame of an textual node.
         *
         * @prop {String} headline — Headline of the frame
         * @prop {String} text — Markdown formatted content
         * @prop {Object} links — Dictionary of links to another frames
         *
         * @param {{
         * id: String,
         * headline: String,
         * text: String,
         * links: Object}} options
         * @constructor
         */
        constructor(options) {
            this.id = options.id || Three.Math.generateUUID();
            this.headline = options.headline || '';
            this.text = options.text || '';
            this.links = options.links || {};

            this.linksUpdated = true;

            this.initHTML();
        }

        /** @param {String} text */
        set text(text) {
            this._text = text;
            this.textUpdated = true;
        }

        /** @returns {String} */
        get text() { return this._text; }

        static get PREVIOUS_LINK() { return 'Previous'; }
        static get md() { return markDownRendererSingleton; }

        /**
         * Constructs a new TextFrame from a serialized JSON representation.
         *
         * @param {Object} json
         * @returns {TextFrame}
         */
        static fromJSON(json) {
            let links = {};
            for (let link of json.links) {
                links[link.name] = link.target;
            }

            json.links = links;

            return new TextFrame(json);
        }

        /**
         * Serializes this TextFrame into IVEditor JSON representation.
         *
         * @returns {{id: String, text: String, links: {name: String, target: String}[]}}
         */
        toJSON() {
            let links = [];
            let keys = Object.keys(this.links);
            for (let key of keys) {
                if (key !== TextFrame.PREVIOUS_LINK) {
                    links.push({name: key, target: this.links[key]});
                }
            }

            return {
                id: this.id,
                text: this.text,
                headline: this.headline,
                links: links
            };
        }

        /**
         * Initializes HTMLElements needed to render the frame on screen.
         */
        initHTML() {
            this.h1 = document.createElement('h1');

            this.section = document.createElement('section');
            this.section.setAttribute('class', 'frame-text');

            this.buttons = document.createElement('div');
            this.buttons.setAttribute('class', 'link-buttons');
        }

        /**
         * Populate the HTMLElements with the contents.
         *
         * @param {function(String, String)} linkHandler — Funtion to be run when a link button is clicked.
         */
        render(linkHandler) {
            this.h1.textContent = this.headline;
            this.renderText();
            this.renderLinks(linkHandler);
        }

        /**
         * Helper function for render.
         */
        renderText() {
            if (this.textUpdated) {
                clearElement(this.section);

                /* Output of MarkdownIt should be safe */
                this.section.innerHTML = TextFrame.md.render(this.text);
                this.textUpdated = false;
            }
        }

        /**
         * Helper function for render.
         *
         * @param {function(String, String)} linkHandler — Funtion to be run when a link button is clicked.
         */
        renderLinks(linkHandler) {
            if (this.linksUpdated) {
                clearElement(this.buttons);

                Object.keys(this.links).forEach(name => this.buttons.appendChild(this.createButton(name, linkHandler)));

                this.linksUpdated = false;
            }
        }

        /**
         * Creates a new HTML button element for a link.
         *
         * @param {String} name — Button label text
         * @param {function(String, String)} linkHandler  — Funtion to be run when a link button is clicked.
         * @returns {Element}
         */
        createButton(name, linkHandler) {
            let button = document.createElement('button');
            button.setAttribute('type', 'button');
            button.setAttribute('data-target-id', this.links[name]);
            button.addEventListener('click', () => linkHandler(name, this.links[name]), false);

            button.appendChild(document.createTextNode(name));

            return button;
        }

        /**
         * Adds or updates an existing link to another frame.
         *
         * @param {String} name — Link name/button label
         * @param {String} target — UUID of the target frame
         */
        setLink(name, target) {
            this.links[name] = target;
            this.linksUpdated = true;
        }

        /**
         * Returns the UUID of the target frame.
         *
         * @param {String} name — Link name/button label
         * @returns {String}
         */
        getLink(name) {
            return this.links[name];
        }

        /**
         * Removes an link to another TextFrame
         *
         * @param {String} name — Link name/button label
         */
        removeLink(name) {
            delete this.links[name];
            this.linksUpdated = true;
        }
    }

    // endregion: TextFrame

    // region: Free standing functions

    /**
     * Converts a Three.js Euler object into xyz-array of degrees.
     * Normalizes values to [0, 360[ range.
     *
     * @param {Three.Euler} euler — Roation object to be converteds
     * @returns {Number[]} – [x, y, z]
     */
    function eulerToJSON(euler) {
        return [(euler.x * IVGlobals.RAD_IN_DEG) % 360,
                (euler.y * IVGlobals.RAD_IN_DEG) % 360,
                (euler.z * IVGlobals.RAD_IN_DEG) % 360];
    }

    /**
     * Converts a Three.js Vector[2,3,4] object into xyzw-array.
     *
     * @param {Three.Vector2 | Three.Vector3 | Three.Color | Three.Vector4} vector – Array like Three JS object
     * @returns {Number[]} – [x, y, z, w]
     */
    function vectorToJSON(vector) {
        let arr = [vector.r || vector.x, vector.g || vector.y];

        if (!(vector instanceof Three.Vector2)) {
            arr.push(vector.b || vector.z);

            if (vector instanceof Three.Vector4) {
                arr.push(vector.w);
            }
        }

        return arr;
    }

    /**
     * Converts an IVEditor's uniform JSON-formatted array into appropriate Three.js Vector object.
     *
     * @param {{name: String, type: String, value: Number[]}} json
     * @returns {Three.Vector2 | Three.Vector3 | Three.Vector4 | Three.Color}
     */
    function JSONToVector(json) {
        if (json.type === 'v2') {
            return new Three.Vector2(json.value[0], json.value[1]);
        } else if (json.type === 'v3') {
            return new Three.Vector3(json.value[0], json.value[1], json.value[2]);
        } else if (json.type === 'v4') {
            return new Three.Vector4(json.value[0], json.value[1], json.value[2], json.value[3]);
        }

        return new Three.Color(json.value[0], json.value[1], json.value[2]);
    }

    /**
     * Converts an IVEditor's uniform JSON-formatted matrix into appropriatee Three.js matrix object.
     *
     * @param {{name: String, type: String, value: Number[]}} json
     * @returns {THREE.Matrix3 | THREE.Matrix4}
     */
    function JSONToMatrix(json) {
        let value = new Three.Math.Matrix3();

        if (json.type === 'm4') {
            value = new Three.Math.Matrix4();
        }

        value.elements = Float32Array.from(json.value);
        return value;
    }

    /**
     * Converts an array of degree values into Three.js Euler object.
     *
     * @param {Number[]} json
     * @returns {THREE.Euler}
     */
    function JSONToEuler(json) {
        return new Three.Euler(json[0] * IVGlobals.PI_PER_180,
                               json[1] * IVGlobals.PI_PER_180,
                               json[2] * IVGlobals.PI_PER_180);
    }

    /**
     * Removes all of the children from the HTMLElement
     *
     * @param {Element} element
     */
    function clearElement(element) {
        while (element.firstChild) {
            element.removeChild(element.firstChild);
        }
    }

    /**
     * Wrapper for an error message string to ensure consistency.
     *
     * @param {XMLHttpRequest} request
     * @returns {String}
     */
    function serverError(request) {
        return `A server error occurred. Error code: ${request.status}`;
    }

    /**
     * Handler for REST API POST requests.
     * Resolves to returned status object if status == ok, else rejects
     * the status object.
     *
     * @param {XMLHttpRequest} request
     * @param {function(*)} resolve – Promise resolve handler
     * @param {function(*)} reject – Promise reject handler
     */
    function postResultHandler(request, resolve, reject) {
        if (request.readyState === 4) {
            if (request.status === 200) {
                let response = JSON.parse(request.responseText);
                if (response.status === 'ok') {
                    resolve(response);
                } else {
                    reject(response);
                }
            } else {
                reject(serverError(request));
            }
        }
    }

    /**
     * Handler for REST API GET requests.
     * Resolves get request to parsed JSON object on success
     * and rejects it on error.
     *
     * @param {XMLHttpRequest} request
     * @param {function(*)} resolve – Promise resolve handler
     * @param {function(*)} reject – Promise reject handler
     */
    function getResultHandler(request, resolve, reject) {
        if (request.readyState === 4) {
            if (request.status === 200) {
                resolve(JSON.parse(request.responseText));
            } else {
                reject(serverError(request));
            }
        }
    }

    /**
     * Send a REST API POST request.
     *
     * @param {String} path
     * @param {*} data
     * @param {String=} type
     * @returns {Promise<(true | String)>} – String on error
     */
    function post(path, data, type) {
        return new Promise((resolve, reject) => {
            let request = new XMLHttpRequest();
            request.open('POST', path);
            request.setRequestHeader('Content-Type', type || 'application/json;charset=UTF-8');
            request.setRequestHeader('Content-Encoding', 'gzip');
            request.send(Pako.gzip(data));
            request.addEventListener('readystatechange', () => postResultHandler(request, resolve, reject));
        });
    }

    /**
     * Send a REST API GET request.
     *
     * @param {String} path
     * @returns {Promise<(Object | String)>} – String on error
     */
    function get(path) {
        return new Promise((resolve, reject) => {
            let request = new XMLHttpRequest();
            request.open('GET', path);
            request.send();
            request.addEventListener('readystatechange', () => getResultHandler(request, resolve, reject));
        });
    }

    // endregion: Free standing functions

    return {
        AssetsLoader: AssetsLoaderSingleton,
        AutomatedAudioNode: AutomatedAudioNode,
        clearElement: clearElement,
        eulerToJSON: eulerToJSON,
        get: get,
        JSONToEuler: JSONToEuler,
        post: post,
        SPBox: SPBox,
        SPCoordinate: SPCoordinate,
        TextFrame: TextFrame,
        TriggeringCounter: TriggeringCounter,
        Uniform: Uniform,
        Uniforms: Uniforms,
        vectorToJSON: vectorToJSON
    };
});
