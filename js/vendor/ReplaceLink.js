/*
 * Based on <https://github.com/martinheidegger/markdown-it-replace-link>
 */

define([], function() {
    'use strict';

    var ReplaceLink = function(md, replace) {
        this.md = md;
        this.md.ReplaceLinkInstance = this;
        this.md.core.ruler.after('inline', 'replace-link', state => this.replaceLink(state));

        this.replace = replace || function(link) { return link; };
    };

    ReplaceLink.prototype.replaceAttr = function(token, attrName, env) {
        for (var attr of token.attrs) {
            if (attr[0] === attrName) {
                attr[1] = this.replace(attr[1], env, token);
            }
        }
    };

    ReplaceLink.prototype.processChildren = function(block, state) {
        for (var token of block.children) {
            var type = token.type;
            if (type === 'link_open') {
                this.replaceAttr(token, 'href', state.env);
            } else if (type === 'image') {
                this.replaceAttr(token, 'src', state.env);
            }
        }
    };

    ReplaceLink.prototype.replaceLink = function(state) {
        for (var blockToken of state.tokens) {
            if (blockToken.type === 'inline' && blockToken.children) {
                this.processChildren(blockToken, state);
            }
        }
    };

    ReplaceLink.apply = function(plugin, args) {
        return new ReplaceLink(args[0], args[1]);
    };

    return ReplaceLink;
});
