/**
 * @author alteredq / http://alteredqualia.com/
 * @author mrdoob / http://mrdoob.com/
 * @author arodic / http://aleksandarrodic.com/
 * @author fonserbc / http://fonserbc.github.io/
*/

define(['three'], function(Three) {
    'use strict';

    return function(renderer) {
        var _stereo = new Three.StereoCamera();
        _stereo.aspect = 0.5;

        this.setSize = function(width, height) {
            renderer.setSize(width, height);
        };

        this.render = function(scene, camera) {
            scene.updateMatrixWorld();

            if (camera.parent === null) {
                camera.updateMatrixWorld();
            }

            _stereo.update(camera);

            var size = renderer.getSize();

            renderer.setScissorTest(true);
            renderer.clear();

            renderer.setScissor(0, 0, size.width / 2, size.height);
            renderer.setViewport(0, 0, size.width / 2, size.height);
            renderer.render(scene, _stereo.cameraL);

            renderer.setScissor(size.width / 2, 0, size.width / 2, size.height);
            renderer.setViewport(size.width / 2, 0, size.width / 2, size.height);
            renderer.render(scene, _stereo.cameraR);

            renderer.setScissorTest(false);
        };
    };
});
