/**
 * Copyright 2015-2016 by Jesse Jaara <jesse.jaara@gmail.com>
 * Copyright 2015-2016 by Jussi Karhu <karhu.jussi.t@student.uta.fi>
 *
 * This file is part of IVEditor.
 *
 * IVEditor is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * IVEditor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with IVEditor.
 * If not, see <http://www.gnu.org/licenses/>.
 *
 * @license GPL-3.0+ <http://spdx.org/licenses/GPL-3.0+>
 */

'use strict';

let fs = require('fs');
let Gettext = require('node-gettext');
let glob = require('glob');

let gt = new Gettext();
for (let file of glob.sync('locale/*.mo')) {
    gt.addTextdomain(file.replace('.mo', ''), fs.readFileSync(file));
}

/**
 * @param {String} context
 * @param {String} msg
 * @param {(String | Number)} plural
 * @param {Number} count
 * @return {String}
 */
function helper(context, msg, plural, count) {
    if (count) {
        return gt.dnpgettext(gt._currentDomain, context, msg, plural, count);
    } else if (plural) {
        return gt.dnpgettext(gt._currentDomain, '', context, msg, plural);
    } else if (msg) {
        return gt.dnpgettext(gt._currentDomain, context, msg);
    }

    return gt.dnpgettext(gt._currentDomain, '', context);
}

/**
 * @returns {function(String, String, (Number|String), (Number|String), (Number|String), (Number|String)): String}
 */
module.exports = helper;
