/**
 * Copyright 2015-2016 by Jesse Jaara <jesse.jaara@gmail.com>
 * Copyright 2015-2016 by Jussi Karhu <karhu.jussi.t@student.uta.fi>
 *
 * This file is part of IVEditor.
 *
 * IVEditor is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * IVEditor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with IVEditor.
 * If not, see <http://www.gnu.org/licenses/>.
 *
 * @license GPL-3.0+ <http://spdx.org/licenses/GPL-3.0+>
 */

'use strict';

let fs = require('fs');
let Buffer = require('buffer').Buffer;
let glob = require('glob-promise');
let notifier = require('node-notifier');
let open = require('opener');
let path = require('path');
let utils = require('./utils.js');
let yazl = require('yazl');

function Project(root) {
    this.root = root;
}

Project.globs  = {
    audios: 'assets/audio/*.{wav,mp3,m4a}',
    images: 'assets/images/*.{ktx,png,jpeg,jpg}',
    models: 'assets/models/*.obj',
    shaders: 'assets/shaders/*/*.glsl',
    skydomes: 'assets/skydomes/*.{ktx,png,jpeg,jpg,mp4}',
    textures: 'assets/textures/*.{ktx,png,jpeg,jpg}'
};

/**
 * @param {String} dir – use dir asd root directory
 * @returns {Promise<{String: String[]}>} – resolves to object with types as keys and list of files as values
 */
Project.listAssets = function(dir) {
    return new Promise(resolve => {
        let promised = [];

        for (let type of Object.keys(Project.globs)) {
            promised.push(new Promise(resolve => {
                glob(Project.globs[type], {cwd: dir}).then(rawList => {
                    var files = {};
                    for (var file of rawList) {
                        files[file.split('/').pop()] = '/' + file;
                    }
                    resolve({type: type, files: files});
                });
            }));
        }

        Promise.all(promised).then(lists => {
            var files = {};
            for (let list of lists) {
                files[list.type] = list.files;
            }

            resolve(files);
        });
    });
};

/**
 * @returns {Promise<{String: String[]}>} – resolves to object with types as keys and list of files as values
 */
Project.prototype.listAssets = function() {
    return Project.listAssets(this.root);
};

Project.prototype.saveAssets = function(files, frontendPath) {
    let streams = [];
    for (let file of files) {
        streams.push(new Promise((resolve, reject) => {
            let ws = fs.createWriteStream(path.join(this.root, file.to));
            ws.on('error', error => reject({status: 'error', message: utils.parseFsError(error)}));
            ws.on('close', () => resolve(ws.path));

            if (file.action === 'copy') {
                let is = fs.createReadStream(path.join(frontendPath, file.from));
                is.on('error', error => reject({status: 'error', message: utils.parseFsError(error)}));
                is.pipe(ws);
            } else {
                ws.end(file.data);
            }
        }));
    }

    return Promise.all(streams);
};

/**
 * Should the file be compressed.
 *
 * @param {String} file
 * @returns {Boolean}
 */
Project.prototype.compressFile = function(file) {
    return (file.match(/\.(glsl|json|ktx|obj|wav)$/i) !== null);
};

Project.prototype.zipFile = function(zf, file) {
    zf.addFile(this.root + '/' + file, file, {compress: this.compressFile(file)});
};

/**
 * Writes the project to the given file in zip format.
 *
 * @returns {Stream}
 */
Project.prototype.zip = function() {
    let zf = new yazl.ZipFile();

    this.zipFile(zf, 'project.json');
    this.listAssets().then(assets => {
        for (let type of Object.keys(assets)) {
            for (let file of assets[type]) {
                this.zipFile(zf, file);
            }
        }

        zf.end();
    });

    zf.outputStream.pipe(fs.createWriteStream(this.root + '/project.zip')).on('close', function() {
        notifier.notify({
            title: 'IVEditor – Exporter',
            message: 'Your project has been exported to a zip bundle.',
            icon: path.join(__dirname, 'assets/IVEditor-notification.png'),
            sound: true,
            wait: () => { open(path.resolve(this.root)); }
        });
    });

};

/**
 * Returns a buffer containing the project.json file of the project.
 * Rejects with fs error if the file doesn't exist.
 *
 * @returns {Promise<(Buffer | {status: String, message: String})>}
 */
Project.prototype.getJSON = function() {
    return new Promise((resolve, reject) => {
        let file = path.join(this.root, 'project.json');
        fs.access(file, fs.R_OK | fs.W_OK, error => {
            if (error) {
                reject({status: 'error', message: utils.parseFsError(error)});
                return;
            }

            let chunks = [];

            let is = fs.createReadStream(file);
            is.on('data', chunk => chunks.push(chunk));
            is.on('error', error => reject({status: 'error', message: utils.parseFsError(error)}));
            is.on('end', () => {
                let buffer = Buffer.concat(chunks);
                resolve(buffer);
            });
        });
    });
};

module.exports = Project;
