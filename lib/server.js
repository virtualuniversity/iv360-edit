/**
 * Copyright 2015-2016 by Jesse Jaara <jesse.jaara@gmail.com>
 * Copyright 2015-2016 by Jussi Karhu <karhu.jussi.t@student.uta.fi>
 *
 * This file is part of IVEditor.
 *
 * IVEditor is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * IVEditor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with IVEditor.
 * If not, see <http://www.gnu.org/licenses/>.
 *
 * @license GPL-3.0+ <http://spdx.org/licenses/GPL-3.0+>
 */

'use strict';

let express = require('express');
let fs = require('fs');
let bodyParser = require('body-parser');
let Buffer = require('buffer');
let morgan = require('morgan');
let path = require('path');
let Project = require('./project.js');
let serveIndex = require('serve-index');
let utils = require('./utils.js');
let zlib = require('zlib');

let __ = require('./gettext-helper.js');
let fmt = require('util').format;

/**
 * @param {{
 * port: Number,
 * frontendPath: String}} options
 * @constructor
 */
function Server(options) {
    this.app = express();
    this.app.use(morgan('dev'));
    this.app.use(bodyParser.json({
        inflate: true,
        limit: '50MB' // All post data is compressed. This should be more than enough
    }));
    this.setupFrontend(options.frontendPath);

    this.httpServer = this.app.listen(options.port);
    this.project = null;

    this.setupRoutes();
}

Server.fileFilter = function(filename, index, files, dir) {
    try {
        if (!fs.lstatSync(path.join(dir, filename)).isDirectory()) {
            return (filename === 'project.json');
        }
    } catch (error) {
        return false;
    }

    return true;
};

/**
 * @param {String} root
 */
Server.prototype.setupFrontend = function(root) {
    this.frontendPath = root;
    for (let dir of ['/assets', '/html', '/js', '/css']) {
        this.app.use(dir, express.static(root + dir));
    }
};

Server.prototype.setProject = function(project) {
    this.project = project;
    if (this.staticProject) {
        for (var i = 0; i < this.app._router.stack.length; i++) {
            if (this.app._router.stack[i].handle === this.staticProject) {
                this.app._router.stack.splice(i,1);
                break;
            }
        }
    }

    this.staticProject = express.static(project.root);
    this.app.use('/project', this.staticProject);

    console.log(`Project root set to ${project.root}`);
};

/**
 * @param {express.request} req
 * @param {express.response} res
 */
Server.prototype.listAssets = function(req, res) {
    console.log(Project);
    let promised = [Project.listAssets(this.frontendPath)];
    if (this.project) {
        promised.push(this.project.listAssets());
    }

    Promise.all(promised).then(assets => res.json({ program: assets[0], project: assets[1] || {} }));
};

/**
 * @param {express.request} req
 * @param {express.response} res
 */
Server.prototype.saveJSON = function(req, res) {
    let ws = fs.createWriteStream(path.join(this.project.root + '/project.json'));

    ws.on('finish', () =>
        res.json({
            status: 'ok',
            message: fmt(__('Wrote project description to file %s'), ws.path)
        })
    );
    ws.on('error', reason => {
        res.json({
            status: 'error',
            message: utils.parseFsError(reason)
        });
    });

    ws.end(req.body.data);
};

/**
 * @param {express.request} req
 * @param {express.response} res
 */
Server.prototype.getJSON = function(req, res) {
    this.project.getJSON().then(buffered => {
        res.json({
            status: 'ok',
            message: fmt(__('Loaded project description file %s'), this.project.root + '/project.json'),
            file: buffered.toString()
        });
    }).catch(reason => res.json(reason));
};

/**
 * @param {express.request} req
 * @param {express.response} res
 */
Server.prototype.saveFiles = function(req, res) {
    this.project.saveAssets(req.body.files, this.frontendPath)
        .then(() => res.json({status: 'ok', message: __('All files saved to the project successfully.')}))
        .catch(reason => res.json(reason));
};

Server.prototype.setProjectRoot = function(req, res) {
    console.log(req.body);
    let path = req.body.rootPath;

    this.setProject(new Project(path));

    res.json({
        status: 'ok',
        message: fmt(__('Project root set to %s'), path)
    });

};

Server.prototype.setupRoutes = function() {
    this.app.use('/browser', serveIndex('/', {
        icons: true,
        filter: Server.fileFilter,
        template: path.resolve(__dirname, '..', 'template', 'browser.html')
    }));

    this.app.get('/rest/assets/list', (req, res) => this.listAssets(req, res));
    this.app.get('/rest/project/zip', (req, res) => {this.project.zip(); res.json({message: 'Zipping started.'})});
    this.app.get('/rest/project/json', (req, res) => this.getJSON(req, res));

    this.app.post('/rest/project/assets/save', (req, res) => this.saveFiles(req, res));
    this.app.post('/rest/project/json', (req, res) => this.saveJSON(req, res));
    this.app.post('/rest/project/root', (req, res) => this.setProjectRoot(req, res));
};

module.exports = Server;
