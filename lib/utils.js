/**
 * Copyright 2015-2016 by Jesse Jaara <jesse.jaara@gmail.com>
 * Copyright 2015-2016 by Jussi Karhu <karhu.jussi.t@student.uta.fi>
 *
 * This file is part of IVEditor.
 *
 * IVEditor is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * IVEditor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with IVEditor.
 * If not, see <http://www.gnu.org/licenses/>.
 *
 * @license GPL-3.0+ <http://spdx.org/licenses/GPL-3.0+>
 */

'use strict';

let fmt = require('util').format;
let __ = require('./gettext-helper.js');

module.exports.parseFsError = function(error) {
    if (error.code === 'EACCES') {
        return fmt(__("You do not have permissions to access file '%s'."), error.path);
    } else if (error.code === 'ENOENT') {
        return fmt(__("File '%s' does not exist."), error.path);
    } else {
        return fmt(__('An unknown filesystem error %s occured. Full error is: %s'), error.code, error);
    }
};
