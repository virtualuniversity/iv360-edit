'use strict';

var Validator = require('validator');
var Three = require('three');
var requirejs = require('requirejs');
requirejs.config(require('../js/RequireJS-config.js').config);

var IVProject = requirejs('IVProject');
var Util = requirejs('Util');

describe('IVProject components\' constructors', function() {
    it('New IVProject.Project', function() {
        var options = {};
        options.name = 'VirtualUniversity';
        options.description =  'A test project for Virtual University';
        options.author = 'Jesse Jaara <jesse.jaara@gmail.com>';
        options.location = 'Tampere';
        options.copyright = 'For private use only (c)';

        var project = new IVProject.Project(options);

        expect(project.name).toBe(options.name);
        expect(project.description).toBe(options.description);
        expect(project.author).toBe(options.author);
        expect(project.location).toBe(options.location);
        expect(project.copyright).toBe(options.copyright);
        expect(Validator.isUUID(project.id, 4)).toBe(true);
    });
});

describe('Scene', function() {
    var options = {};

    var testNodes = [];

    beforeEach(function() {
        testNodes = [];
        for (var i = 1; i <= 5; i++) {
            options.name = 'Test node ' + i.toString();
            testNodes.push(new IVProject.ContentNode(options));
        }
    });

    it('Add content nodes', function() {
        var scene = new IVProject.Scene();

        scene.addContentNode(testNodes[0]);
        scene.addContentNode(testNodes[1]);
        scene.addContentNode(testNodes[2]);

        let nodes = Array.from(scene.nodes, pair => {
            return pair[1];
        });

        expect(scene.nodes.size).toBe(3);
    });

    it('Add invalid nodes', function() {
        var scene = new IVProject.Scene();

        testNodes[1] = {tag: 'my dummy object', what: 'this should fail adding', here: 'or there'};

        scene.addContentNode(testNodes[0]);
        scene.addContentNode(testNodes[1]);
        scene.addContentNode(testNodes[2]);

        expect(scene.nodes.size).toBe(2);
    });

    it('Remove content nodes', function() {
        var scene = new IVProject.Scene();

        for (let node of testNodes) {
            scene.addContentNode(node);
        }

        scene.removeContentNode(testNodes[1]);
        scene.removeContentNode(testNodes[2]);
        scene.removeContentNode(testNodes[3]);

        expect(scene.nodes.size).toBe(2);
    });
});
