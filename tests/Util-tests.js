'use strict';

var NodeUtil = require('util');
var requirejs = require('requirejs');
requirejs.config(require('../js/RequireJS-config.js').config);
var Three = requirejs('three');
var Util = requirejs('Util');

function generateSphericalToCartesianTests(tests) {
    tests.forEach(function(test) {
        testSphericalToCartesian(test.r, test.z, test.a, test.X, test.Y, test.Z);
    });
}

function testSphericalToCartesian(r, zenith, azimuth, expectedX, expectedY, expectedZ) {
    var fmtstr = 'convert spherical to cartesian: (%d, %d°, %d°) -> (%d, %d, %d)';
    it(NodeUtil.format(fmtstr, r, zenith, azimuth, expectedX, expectedY, expectedZ), function() {
        var cartesian = new Util.SPCoordinate(r, zenith, azimuth).toCartesian();

        expect(cartesian.x).toBeCloseTo(expectedX, 3);
        expect(cartesian.y).toBeCloseTo(expectedY, 3);
        expect(cartesian.z).toBeCloseTo(expectedZ, 3);
    });
}

function generateCartesianToSphericalTests(tests) {
    tests.forEach(function(test) {
        testCartesianToSpherical(test.X, test.Y, test.Z, test.r, test.z, test.a);
    });
}

function testCartesianToSpherical(x, y, z, expectedRadius, expectedZenith, expectedAzimuth) {
    var fmtstr = 'convert cartesian to SPCoordinate: (%d, %d, %d) -> (%d, %d°, %d°)';
    it(NodeUtil.format(fmtstr, x, y, z, expectedRadius, expectedZenith, expectedAzimuth), function() {
        var spherical = Util.SPCoordinate.fromCartesian(new Three.Vector3(x, y, z))

        expect(spherical.radius).toBeCloseTo(expectedRadius, 3);
        expect(spherical.zenith).toBeCloseTo(expectedZenith, 3);

        // If we are testing polar points the azimuth value does not matter
        if (spherical.y < 90 && spherical.y > -90) {
            expect(spherical.azimuth).toBeCloseTo(expectedAzimuth, 3);
        }
    });
}

function runCartesianSphericalTests(fn) {
    for (var i = 0; i < 2; i++) {
        var r = 1;
        if (i === 1) { // Make sure the function also works correctly with non unit spheres
            r = 7;
        }

        // Test all 6 corner points. Should be sufficient
        fn([
            {r: r, z: -90, a:   0, X:  0, Y: -r, Z:  0},
            {r: r, z:  90, a:   0, X:  0, Y:  r, Z:  0},
            {r: r, z:   0, a:   0, X:  r, Y:  0, Z:  0},
            {r: r, z:   0, a:  90, X:  0, Y:  0, Z:  r},
            {r: r, z:   0, a: 180, X: -r, Y:  0, Z:  0},
            {r: r, z:   0, a: 270, X:  0, Y:  0, Z: -r},
            {r: r, z:  90, a: 270, X:  0, Y:  r, Z:  0}
        ]);
    }
}

describe('Test utility class SPCoordinate', function() {
    runCartesianSphericalTests(generateSphericalToCartesianTests);
    runCartesianSphericalTests(generateCartesianToSphericalTests);
});

describe('Test utility class TriggeringCounter', function() {
    it('count down', function() {
        var finished = false;
        var i = 0;

        var counter = new Util.TriggeringCounter(10, () => finished = i);

        for (; i < 20; i++) {
            if (i < 5 || i >= 10) {
                counter.up();
            } else {
                counter.down();
            }
        }

        expect(finished).toBe(19);
    });

    it('count up', function() {
        var finished = false;
        var i = 0;

        var counter = new Util.TriggeringCounter(10, () => finished = i);

        for (; i < 10; i++) {
            counter.up();
        }

        expect(finished).toBe(9);
    });
});
